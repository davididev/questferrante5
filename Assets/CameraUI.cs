﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CameraUI : MonoBehaviour
{
    public Image healthBarOverlay, magicBarOverlay, xpBarOverlay;
    public Animator healthBarAnim;

	public GameObject worldMapArea, worldMapButton, shopPanel, keyPanel;
    public TMPro.TextMeshProUGUI healthText, magicText, xpText, goldText, worldMapAreaText, keyText;

	public string worldMapText = "";


    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void ShowShop()
	{
		shopPanel.SetActive(true);
	}

    // Update is called once per frame
    void Update()
    {
        int xp = GameDataHolder.instance.GetCurrentHero().XP;
        int hp = PlayerHealth.minHealth;
        int mp = PlayerHealth.minMagic;
		int gold = GameDataHolder.instance.GetCurrentHero().gold;
        float xpPerc = (xp) / 100f;
        if (healthBarAnim != null)
            healthBarAnim.SetBool("Low", PlayerHealth.healthPerc <= 0.25f);

        healthBarOverlay.fillAmount = PlayerHealth.healthPerc;
        magicBarOverlay.fillAmount = PlayerHealth.magicPerc;
        xpBarOverlay.fillAmount = xpPerc;

        xpText.text = xp.ToString();
        healthText.text = hp.ToString();
        magicText.text = mp.ToString();
        goldText.text = gold.ToString("D7");
		
		if(SceneVars.keyVarID != -1)
		{
			keyPanel.SetActive(true);
			keyText.text = SceneVars.keyCount.ToString();
		}
		else
			keyPanel.SetActive(false);
		
		if(worldMapText != "")
		{
            if (DialogueHandler.IS_RUNNING)  //Currently teleporting
            {
                worldMapArea.SetActive(false);
                worldMapText = "";
            }
            else
                worldMapArea.SetActive(true);
            
			if(worldMapText == "LOCKED")
			{
				worldMapButton.SetActive(SceneVars.keyCount > 0);
			}
			else
				worldMapButton.SetActive(true);
			worldMapAreaText.text = worldMapText;
			if(Time.timeScale > 0.1f)  //Not paused
			{
				EventSystem.current.SetSelectedGameObject(worldMapButton);
			}
		}
		else
		{
			worldMapArea.SetActive(false);
		}
		
    }
	
	public void GoToArea()
	{
		if(TeleportRegion.LockID != -1)
		{
			if(worldMapText == "LOCKED")
			{
				GameDataHolder.instance.gameVars[SceneVars.keyVarID] -= 1;  //Subtract a key
				char[] ca = GameDataHolder.instance.locks.ToCharArray();
				ca[TeleportRegion.LockID] = '1';  //Set door to unlocked
				GameDataHolder.instance.locks = new string(ca);
			}
		}
		FindObjectOfType<DialogueHandler>().StartEvent(TeleportRegion.MyEvent);
		worldMapText = "";
	}
}
