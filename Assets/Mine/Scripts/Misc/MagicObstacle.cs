﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicObstacle : MonoBehaviour
{
	public DamageMessage.ELEMENT[] elementsToBreak;
	public enum DEATH_TYPE { ScaleVertical, ScaleWhole, Fade };
	public AudioClip deathSound;
	
	public DEATH_TYPE deathType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	SpriteRenderer rend;
	void SetColor(Color c)
	{
		if(rend == null)
			rend = GetComponent<SpriteRenderer>();
		
		rend.color = c;
	}
	
	void SetScale(Vector3 s)
	{
		transform.localScale = s;
	}
	
	void BreakMe()
	{
		if(deathSound != null)
			AudioSource.PlayClipAtPoint(deathSound, Camera.main.transform.position);
		float t = 1f;
		if(deathType == DEATH_TYPE.ScaleVertical)
			iTween.ValueTo(gameObject, iTween.Hash("from", new Vector3(1f, 1f, 1f), "to", new Vector3(1f, 0f, 1f), "time", t, "onupdate", "SetScale", "oncomplete", "DisableMe"));
		if(deathType == DEATH_TYPE.ScaleWhole)
			iTween.ValueTo(gameObject, iTween.Hash("from", new Vector3(1f, 1f, 1f), "to", new Vector3(0f, 0f, 0f), "time", t, "onupdate", "SetScale", "oncomplete", "DisableMe"));
		if(deathType == DEATH_TYPE.Fade)
			iTween.ValueTo(gameObject, iTween.Hash("from", Color.white, "to", new Color(1f, 1f, 1f, 0f), "time", t, "onupdate", "SetColor", "oncomplete", "DisableMe"));
	}
	
	void DisableMe()
	{
		gameObject.SetActive(false);
	}
	
	/// <summary>
    /// Should be called by GameObject.SendMessage.
    /// </summary>
    /// <param name="msg"></param>
	public void Damage(DamageMessage msg)
	{
		bool failure = true;
		for(int i = 0; i < elementsToBreak.Length; i++)
		{
			if(msg.ElementID == elementsToBreak[i])
				failure = false;
		}
		
		if(failure == false)
		{
			BreakMe();
		}
	}
}
