﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimOnEnable : MonoBehaviour
{
	public string trigger = "Play";
	public Animator anim;
	public float disappearTime;
    // Start is called before the first frame update
    void OnEnable()
    {
		anim.SetTrigger(trigger);
		Invoke("DisableMe", disappearTime);
    }
	
	void DisableMe()
	{
		gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
