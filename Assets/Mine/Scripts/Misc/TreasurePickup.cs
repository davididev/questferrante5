﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasurePickup : MonoBehaviour
{
	public int treasureID = 0;
	public int itemCount = 1;
	public char itemID = 'f';
	public AudioClip getKeySound;
	
    // Start is called before the first frame update
    void Start()
    {
        char c = GameDataHolder.instance.chests[treasureID];
		if(c == '1')
			gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			AudioSource.PlayClipAtPoint(getKeySound, Camera.main.transform.position);
			char[] myArray = GameDataHolder.instance.chests.ToCharArray();
			myArray[treasureID] = '1';
			GameDataHolder.instance.chests = new string(myArray);
			
			GameObject txt = GameObjectPool.GetInstance("DMG", transform.position + (Vector3.left * 2f), Quaternion.identity);
			txt.SendMessage("ShowText", ("<color=white>+" + itemCount + " " + ItemDB.instance.GetItemName(itemID)) );
			
			GameDataHolder.instance.AddItem(itemID, itemCount);
			gameObject.SetActive(false);
		}
	}
}
