﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndOfBeta : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(FindObjectOfType<BoilerRoot>().gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	
	//Just so we don't have to create a canvas in the beta.
	public void ReturnToTitle()
	{
		SceneManager.LoadScene("Title");
	}
}
