﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickup : MonoBehaviour
{
	public int keyID = 0;
	public AudioClip getKeySound;
    // Start is called before the first frame update
    void Start()
    {
        char c = GameDataHolder.instance.keys[keyID];
		if(c == '1')
			gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			AudioSource.PlayClipAtPoint(getKeySound, Camera.main.transform.position);
			GameDataHolder.instance.gameVars[SceneVars.keyVarID] += 1;
			char[] myArray = GameDataHolder.instance.keys.ToCharArray();
			myArray[keyID] = '1';
			GameDataHolder.instance.keys = new string(myArray);
			
			GameObject txt = GameObjectPool.GetInstance("DMG", transform.position + (Vector3.left * 2f), Quaternion.identity);
			txt.SendMessage("ShowText", ("<color=white>+" + "Got a key!"));
			gameObject.SetActive(false);
		}
	}
}
