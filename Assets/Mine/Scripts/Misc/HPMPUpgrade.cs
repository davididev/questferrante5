﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPMPUpgrade : MonoBehaviour
{
	public int upgradeID = 0;
	public bool upgradeHP = true;
	public AudioClip upgradeSound;
    // Start is called before the first frame update
    void Start()
    {
        if(GetUpgradeChar() == '1')
			gameObject.SetActive(false);
    }
	
	private char GetUpgradeChar()
	{
		return GameDataHolder.instance.healthMagicUpgrades[upgradeID];
	}
	
	private void SetUpgradeChar(char c)
	{
		char[] s = GameDataHolder.instance.healthMagicUpgrades.ToCharArray();
		s[upgradeID] = c;
		GameDataHolder.instance.healthMagicUpgrades = new string(s);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			if(upgradeSound != null)
				AudioSource.PlayClipAtPoint(upgradeSound, Camera.main.transform.position);
			SetUpgradeChar('1');
			if(upgradeHP == true) //HP upgrade
			{
				GameObject txt = GameObjectPool.GetInstance("DMG", transform.position, Quaternion.identity);
				txt.SendMessage("ShowText", ("<color=red>" + "HP upgrade"));
				GameDataHolder.instance.GetCurrentHero().hpUpgrades++;
				GameDataHolder.instance.GetCurrentHero().minHP = GameData.GetMaxHP(GameDataHolder.instance.GetCurrentHero().hpUpgrades);
			}
			else //MP upgrade
			{
				GameObject txt = GameObjectPool.GetInstance("DMG", transform.position, Quaternion.identity);
				txt.SendMessage("ShowText", ("<color=blue>" + "MP upgrade"));
				GameDataHolder.instance.GetCurrentHero().mpUpgrades++;
				GameDataHolder.instance.GetCurrentHero().minMP = GameData.GetMaxMP(GameDataHolder.instance.GetCurrentHero().mpUpgrades);
			}
			
			gameObject.SetActive(false);
		}
	}
}
