﻿using UnityEngine;
using System.Collections;

public class PlayParticlesOnce : MonoBehaviour {

	[SerializeField] private ParticleSystem[] particles;
    [SerializeField] private AudioClip sound;
    private AudioSource src;
	private float maxTime = 0f;

	// Use this for initialization
	void OnEnable () {
        src = GetComponent<AudioSource>();
        if(src == null)  //Couldn't get a component, let's make a default one
        {
            src = gameObject.AddComponent<AudioSource>();
            src.minDistance = 5f;
            src.maxDistance = 40f;
        }
        maxTime = 0f;
        if (sound != null)
        {
            src.clip = sound;
            maxTime = sound.length;
        }
        
        foreach (ParticleSystem ps in particles) {
            ps.Play();
            
            GetMaxTime(ps);
		}

        Invoke("Disable", maxTime);
	}

    void Disable()
    {
        gameObject.SetActive(false);
    }

	void GetMaxTime(ParticleSystem p)
	{
        float myMax = p.duration + p.startLifetime;
        if (myMax > maxTime)
            maxTime = myMax;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
