﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// When this object is enabled, it will turn itself off over lifeTime.
/// Good for temporary objects instantiated in a Game Object Pool, such as
/// particle effects.
/// </summary>
public class DisableOverTime : MonoBehaviour {

    public float lifeTime = 1f;
	public string animTriggerOnEnable = "";

    private void OnEnable()
    {
		if(animTriggerOnEnable != "")
		{
			GetComponent<Animator>().SetTrigger(animTriggerOnEnable);
		}
        StartCoroutine(DisableMe());
    }

    IEnumerator DisableMe()
    {
        yield return new WaitForSeconds(lifeTime);
        gameObject.SetActive(false);
    }
}
