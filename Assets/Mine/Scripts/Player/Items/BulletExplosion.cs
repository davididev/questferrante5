﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletExplosion : MonoBehaviour
{
    public AudioClip swingFx, hitFX;
    public int weaponPower = 5;
    public DamageMessage.ELEMENT element = DamageMessage.ELEMENT.Weapon;
    public float weaponTimeMultiplier = 1f;

    private Animator anim;
    private int damageAmount;

    // Start is called before the first frame update
    void Boom()
    {
        damageAmount = weaponPower * (GameDataHolder.instance.GetCurrentHero().GetDamageMultiplier());
        if (swingFx != null)
            AudioSource.PlayClipAtPoint(swingFx, Camera.main.transform.position);
        anim = GetComponent<Animator>();
        if (anim != null)
            anim.SetTrigger("Attack");

        Invoke("DisableMe", 0.65f * weaponTimeMultiplier);
    }

    void DisableMe()
    {
        gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int layer = collision.gameObject.layer;
        if (LayerMask.LayerToName(layer) == "Enemy")
        {
            collision.attachedRigidbody.gameObject.SendMessage("Damage", new DamageMessage(damageAmount, element, transform.localScale.normalized.x * weaponPower), SendMessageOptions.DontRequireReceiver);
        }
    }
}
