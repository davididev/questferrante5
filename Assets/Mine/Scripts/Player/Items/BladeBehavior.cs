﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeBehavior : MonoBehaviour
{
	public AudioClip swingFx, hitFX;
	private Animator anim;

    public int weaponPower = 2;
    public DamageMessage.ELEMENT element = DamageMessage.ELEMENT.Weapon;
	public bool drainHP = false;

    private int damageAmount;
	public float weaponTimeMultiplier = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

	public void InitItem(ItemMSG msg)
	{
		Debug.Log("Mult: " + GameDataHolder.instance.GetCurrentHero().GetDamageMultiplier());
        damageAmount = weaponPower * (GameDataHolder.instance.GetCurrentHero().GetDamageMultiplier());
        if (swingFx != null)
			AudioSource.PlayClipAtPoint(swingFx, Camera.main.transform.position);
		anim = GetComponent<Animator>();
		
		if(anim != null)
			anim.SetTrigger("Attack");
		
		transform.parent = msg.player.transform;
		transform.localPosition = new Vector3(0f, 1f, 0f);
		Vector3 scale = Vector3.one;
		scale.x = (msg.facingLeft) ? 1f : -1f;
		
		transform.localScale = scale;
		Invoke("DisableMe", 0.65f * weaponTimeMultiplier);
	}
	
	void DisableMe()
	{
		transform.parent = null;
		gameObject.SetActive(false);
	}
	

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int layer = collision.gameObject.layer;
        if (LayerMask.LayerToName(layer) == "Enemy")
        {
            if (hitFX != null)
                AudioSource.PlayClipAtPoint(hitFX, Camera.main.transform.position);
            collision.attachedRigidbody.gameObject.SendMessage("Damage", new DamageMessage(damageAmount, element, transform.localScale.normalized.x * weaponPower), SendMessageOptions.DontRequireReceiver);
			
			if(drainHP == true)
			{
				PlayerHealth hp = FindObjectOfType<PlayerHealth>();
				int amt = GameData.GetMaxHP(GameDataHolder.instance.GetCurrentHero().hpUpgrades) * 5 / 100;
				hp.Damage(new DamageMessage(-amt, DamageMessage.ELEMENT.Weapon));
			}
        }
        
    }
}
