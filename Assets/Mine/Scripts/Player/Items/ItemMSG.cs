﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMSG 
{
    public GameObject player;
	public bool facingLeft;
	public int handID;
	
	public ItemMSG(GameObject p, bool f, int i)
	{
		player = p;
		facingLeft = f;
		handID = i;
	}
}
