﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public TrailRenderer trailRend;
    public AudioClip throwFX, hitFX;
    public float horizontalVelocity = 5f;
    public float gravityScale = 0f;

    private bool facingLeft = false;
    private float gravity = 0f;

    public int weaponPower = 2;
    public DamageMessage.ELEMENT element = DamageMessage.ELEMENT.Weapon;

    private int damageAmount;

    public GameObject explosionPrefab;
    public string explosionPrefabName = "";

    public void InitItem(ItemMSG msg)
    {
        if (explosionPrefab != null)
            GameObjectPool.InitPoolItem(explosionPrefabName, explosionPrefab, 15);
        if (throwFX != null)
            AudioSource.PlayClipAtPoint(throwFX, Camera.main.transform.position);
        damageAmount = weaponPower * (GameDataHolder.instance.GetCurrentHero().GetDamageMultiplier());
        this.facingLeft = msg.facingLeft;
        Vector3 scale = transform.localScale;
        if (facingLeft)
            scale.x = Mathf.Abs(scale.x);
        else
            scale.x = -Mathf.Abs(scale.x);

        transform.localScale = scale;
        gravity = 0f;
    }
    private Rigidbody2D rigid;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
        Vector2 vel = new Vector2(0f, 0f);
        gravity += Physics2D.gravity.y * gravityScale * Time.fixedDeltaTime;

        if (facingLeft)
            vel.x = horizontalVelocity;
        else
            vel.x = -horizontalVelocity;

        vel.y = gravity;

        rigid.velocity = vel;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        int layer = collision.gameObject.layer;
        if (LayerMask.LayerToName(layer) == "Default")
        {
            if (hitFX != null)
                AudioSource.PlayClipAtPoint(hitFX, Camera.main.transform.position);

            if (trailRend != null)
                trailRend.Clear();

            if (explosionPrefab != null)
            {
                GameObject boom = GameObjectPool.GetInstance(explosionPrefabName, transform.position, Quaternion.identity);
                boom.SendMessage("Boom");
            }
                
             
            gameObject.SetActive(false);
        }
            

        if(LayerMask.LayerToName(layer) == "Enemy")
        {

            if (trailRend != null)
                trailRend.Clear();
            if (hitFX != null)
                AudioSource.PlayClipAtPoint(hitFX, Camera.main.transform.position);
            collision.collider.gameObject.SendMessage("Damage", new DamageMessage(damageAmount, element, transform.localScale.normalized.x * weaponPower), SendMessageOptions.DontRequireReceiver);

            if (explosionPrefab != null)
            {
                GameObject boom = GameObjectPool.GetInstance(explosionPrefabName, transform.position, Quaternion.identity);
                boom.SendMessage("Boom");
            }
            gameObject.SetActive(false);
        }
    }
}
