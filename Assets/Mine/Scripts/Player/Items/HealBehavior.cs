﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealBehavior : MonoBehaviour
{
	public int healHP = 0, healMP = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

	public void InitItem(ItemMSG msg)
	{
        PlayerHealth hp = FindObjectOfType<PlayerHealth>();
		
        if(hp != null)
        {
            int itemID = GameDataHolder.instance.GetCurrentHero().handItems[msg.handID];
            char itemC = GameDataHolder.instance.GetCurrentHero().items[itemID].id;
            if(healHP != 0)
            {
                hp.Damage(new DamageMessage(-healHP, DamageMessage.ELEMENT.Weapon));
                if (PlayerHealth.DamageError == true)  //HP maxed out- give item back
                {
                    GameDataHolder.instance.AddItem(itemC, 1);
                    gameObject.SetActive(false);
                }
                else
                {
                    GameObject txt = GameObjectPool.GetInstance("DMG", transform.position, Quaternion.identity);
                    txt.SendMessage("ShowText", ("<color=blue> +"+ (-healHP) + " HP"));
                }
            }
            if (healMP != 0)
            {            
                if (hp.OffsetMagic(healMP) == false)  //MP maxed out- give item back
                {
                    GameDataHolder.instance.AddItem(itemC, 1);
                    gameObject.SetActive(false);
                }
                else
                {
                    GameObject txt = GameObjectPool.GetInstance("DMG", transform.position, Quaternion.identity);
                    txt.SendMessage("ShowText", ("<color=green> +" + healMP + " MP"));
                }
            }
            
        }
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
