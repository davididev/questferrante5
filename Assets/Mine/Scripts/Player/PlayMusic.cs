﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    private AudioSource[] srcs;
    private int lastSrcID = 0;
    public static string lastSongName = "";

    // Start is called before the first frame update
    void OnEnable()
    {
        lastSongName = "";
        srcs = new AudioSource[2];
        for(int i = 0; i < srcs.Length; i++)
        {
            srcs[i] = gameObject.AddComponent<AudioSource>();
            srcs[i].loop = true;
            srcs[i].minDistance = 1f;
        }
    }

    public void PlaySong(AudioClip clip)
    {
        if (clip.name == lastSongName)
            return;
        srcs[lastSrcID].Stop();

        if (lastSrcID == 0)
            lastSrcID = 1;
        else
            lastSrcID = 0;

        srcs[lastSrcID].clip = clip;
        srcs[lastSrcID].Play();
        lastSongName = clip.name;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
