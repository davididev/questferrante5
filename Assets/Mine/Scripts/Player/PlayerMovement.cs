﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Tilemaps;

public class PlayerMovement : MonoBehaviour
{
	public static bool ExtraSpoils = false;
	public AudioClip boss1BattleClip;
    public GameObject damageTextPrefab, levelUpPanel, deadPrefab;
	public AudioClip jumpFX;
	public const float MAX_MOVEMENT = 5f, MOVE_SPEED = 120f, JUMP_FORCE = 1800f, WMAP_VELOCITY = 2f;
	public float moveSpeedScale  = 1f;
	public const float STEPS_TO_ENCOUNTER = 8f;
	public Collider2D colliderSideScroll, colliderWmap;
	private Rigidbody2D rigid;
	private SpriteRenderer rend;
	private Animator anim;
	public PauseMenu pauseMenu;
	
	public static bool LockScreen = false;
	
    // Start is called before the first frame update
    void OnEnable()
    {
        InitPoolForWeapons();
		SceneManager.activeSceneChanged += SceneChanged;
		
		if(GameDataHolder.instance.GetCurrentHero().currentScene == SceneManager.GetActiveScene().name)
		{
			Vector3 v = new Vector3(GameDataHolder.instance.GetCurrentHero().currentPosition.x, GameDataHolder.instance.GetCurrentHero().currentPosition.y, 0f);
			transform.position = v;
		}
    }
	
	void OnDestroy()
	{
		SceneManager.activeSceneChanged -= SceneChanged;
		LockScreen = false;
	}

	public void Chapter2()
	{
		Debug.Log("Start chapter 2.");
		StartCoroutine(RoutineNewChapter(1,"CastleFerrante1", Vector3.zero));
	}
    public void Chapter3()
    {
        //SceneManager.LoadScene("EndOfBeta");
		StartCoroutine(RoutineNewChapter(0,"WorldMap", new Vector3(-2.5f, 8f, 0f)));
    }
	public void Chapter4()
    {
        //SceneManager.LoadScene("EndOfBeta");
		StartCoroutine(RoutineNewChapter(1,"WorldMap", new Vector3(0, 0f, 0f)));
    }
	public void Chapter5()
    {
        StartCoroutine(RoutineNewChapter(0,"WorldMap", new Vector3(-25f, -10f, 0f)));
    }

	public void Chapter6()
	{
		StartCoroutine(RoutineNewChapter(1, "WorldMap", new Vector3(-15.5f, -10f, 0f)));
	}

	public void Chapter7()
    {
		SceneManager.LoadScene("EndOfBeta");
	}

	IEnumerator RoutineNewChapter(int heroID,string sceneName, Vector3 position)
	{
		while(DialogueHandler.IS_RUNNING)
		{
			yield return new WaitForEndOfFrame();
		}
		GameData.Hero thisHero = GameDataHolder.instance.beckyStats;
		if(heroID == 1)
			thisHero = GameDataHolder.instance.johnnyStats;
		
		thisHero.currentScene = sceneName;
		thisHero.currentPosition = position;
		GameDataHolder.instance.currentHero = heroID;
		DialogueTeleportActor[] tempDia = new DialogueTeleportActor[1];
		tempDia[0] = new DialogueTeleportActor();
		tempDia[0].sceneToMoveTo = sceneName;
		tempDia[0].positionToMoveTo = position;
		
		FindObjectOfType<DialogueHandler>().StartEvent(tempDia);
	}

    public void AddXP(int xp, int goldOnDeath)
    {
		if(ExtraSpoils)
		{
			xp = xp * 4;
			if(xp > 99)
				xp = 99;
			goldOnDeath = goldOnDeath * 4;
		}
		if(xp > 0)
		{
			GameObject txt = GameObjectPool.GetInstance("DMG", transform.position + (Vector3.left * 2f), Quaternion.identity);
			txt.SendMessage("ShowText", ("<color=yellow>+" + xp + " EXP"));
		}
		
		if(goldOnDeath > 0)
		{
			GameObject txt2 = GameObjectPool.GetInstance("DMG", transform.position + (Vector3.right * 2f), Quaternion.identity);
			txt2.SendMessage("ShowText", ("<color=orange>+" + goldOnDeath + " Gold"));
		}

		GameDataHolder.instance.GetCurrentHero().gold += goldOnDeath;
        GameDataHolder.instance.GetCurrentHero().XP += xp;

        if(GameDataHolder.instance.GetCurrentHero().XP >= 100)
        {
            //GameDataHolder.instance.GetCurrentHero().XP -= 100;
            //

            levelUpPanel.SetActive(true);
        }
    }

	void OnDeath()
	{
		GameObject d = GameObjectPool.GetInstance("Ded", transform.position, Quaternion.identity);
		
		
		StartCoroutine(ReturnToTitle());
		
	}
	
	public void SetScreenLocked(int i)
	{
		if(i > 0)
			LockScreen = true;
		if(i == 0)
			LockScreen = false;
		
		if(i == 2) //Regular Boss battle
			FindObjectOfType<SceneVars>().PlayMusic(boss1BattleClip);
	}
	
	IEnumerator ReturnToTitle()
	{
		float t = 1f;
		PauseMenu.lockPauseMenu = true;
		while(t > 0)
		{
			rend.color = Color.clear;
			t -= Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		PauseMenu.lockPauseMenu = false;
		SceneManager.LoadScene("Title");
	}

    void SceneChanged(Scene s1, Scene s2)
	{
        Debug.Log("Scene changed LOL.");
		Invoke("InitPoolForWeapons", 0.1f);
		
		
	}

	//Should be called when equipment changes and when the scene is changed.
	public void InitPoolForWeapons()
	{
		encounterSteps = 0f;
		lastPos = Vector2.zero;
		string item1 = GetItemStr(0);
		string item2 = GetItemStr(1);
		
		Debug.Log("Item IDS: " + item1 + "," + item2);
        
        GameObject g3 = Resources.Load<GameObject>("LowerItems/Item0") as GameObject;
        GameObject g1;
        GameObject g2; 

        if (item1.ToLower() == item1)
            g1 = Resources.Load<GameObject>("LowerItems/Item" + item1) as GameObject;
        else
            g1 =Resources.Load<GameObject>("Items/Item" + item1) as GameObject;
        if (item2.ToLower() == item2)
            g2 = Resources.Load<GameObject>("LowerItems/Item" + item2) as GameObject;
        else
            g2 = Resources.Load<GameObject>("Items/Item" + item2) as GameObject;
        GameObjectPool.InitPoolItem("Item" + item1, g1, 10);
		GameObjectPool.InitPoolItem("Item" + item2, g2, 10);
        GameObjectPool.InitPoolItem("Item0", g3, 10);
		GameObjectPool.InitPoolItem("Ded", deadPrefab, 10);
		
		if(damageTextPrefab != null)
			GameObjectPool.InitPoolItem("DMG", damageTextPrefab, 20);
    }

	void Update()
	{
		TogglePause();
	}

	private bool test = false;
    // Update is called once per frame
    void FixedUpdate()
    {
            
		if(attackHeldDown > 0f)
			attackHeldDown -= Time.fixedDeltaTime;
        if(rigid == null)
			rigid = GetComponent<Rigidbody2D>();
		if(anim == null)
			anim = GetComponent<Animator>();
		if(rend == null)
			rend = GetComponent<SpriteRenderer>();

        if (DialogueHandler.IS_RUNNING)
            rigid.velocity = Vector2.zero;
		if(ShopMenu.Active == true)
			rigid.velocity = Vector2.zero;

		if(CameraMovement.isWorldMap)
			MovementWMap();
		else
			MovementSide();
		Attacks();
		
		
		
    }
	
	private bool firstPause = true;
	void TogglePause()
	{
		if(DialogueHandler.IS_RUNNING == true)
			return;
        if (PauseMenu.lockPauseMenu)
            return;
		if(ShopMenu.Active)
			return;
		
		if(CrossPlatformInputManager.GetAxis("Pause") == 0f)
		{
			firstPause = true;
		}
		
		if(firstPause == true && CrossPlatformInputManager.GetAxis("Pause") != 0f)
		{
			firstPause = false;
			
			
			if(Mathf.Approximately(Time.timeScale, 1f))
			{
				pauseMenu.gameObject.SetActive(true);
				Time.timeScale = 1f/360f;
				return;
			}
		}
		
		
	}
	
	private int selectedItemID = -1;
	string GetItemStr(int handID)
	{
		selectedItemID = GameDataHolder.instance.GetCurrentHero().handItems[handID];
		string itemID = "0";
		if(selectedItemID > -1)
			itemID = GameDataHolder.instance.GetCurrentHero().items[selectedItemID].id.ToString();
		
		return itemID;
	}
	
	//if id = 0, left hand.  Otherwise, right.
	void AttackID(int handID)
	{
		if(SceneVars.NoWeapons == true)
			return;
		attackHeldDown = 0.6f;
		if(handID == 0)
			anim.SetTrigger("Attack1");
		if(handID == 1)
			anim.SetTrigger("Attack2");
		
		
		string itemID = GetItemStr(handID);
		
		
		
		
		char c = itemID[0];

        int mpUsage = ItemDB.instance.GetItemMP(c);
        if (mpUsage > 0)
        {
            if (GameDataHolder.instance.GetCurrentHero().minMP >= mpUsage)
            {
                GetComponent<PlayerHealth>().OffsetMagic(-mpUsage);
            }
            else
                return;  //Don't have enough MP!
        }

		if(ItemDB.instance.GetItemReusable(c) == false)  //Consumable
		{
			GameData.Item it = GameDataHolder.instance.GetCurrentHero().items[selectedItemID];
            if (it.count == 0) //You ran out.  Set to fist.
			{
				it.id = '0';
                itemID = "0";
                InitPoolForWeapons();
				GameDataHolder.instance.GetCurrentHero().handItems[handID] = -1;
			}
            it.count -= 1;
            GameDataHolder.instance.GetCurrentHero().items[selectedItemID] = it;
		}

        GameObject wep = GameObjectPool.GetInstance("Item" + itemID, transform.position, Quaternion.identity);
        if(wep != null)
			wep.SendMessage("InitItem", new ItemMSG(gameObject, rend.flipX, handID));
        //Debug.Log("Item used: " + itemID);
		
	}
	
	
	float attackHeldDown = 0f;
	void Attacks()
	{
		if(attackHeldDown <= 0f)
		{
			if(CrossPlatformInputManager.GetAxis("Fire1") != 0f)
				AttackID(0);
			if(CrossPlatformInputManager.GetAxis("Fire2") != 0f)
				AttackID(1);
		}
	}
	
	public static WorldMapTile.TERRAIN_TYPE CurrentTerrain;
	
	private Tilemap tileref;
	
	private float encounterSteps = 0f;
	private Vector2 lastPos;
	
	void ToBattle()
	{
        if (DialogueHandler.IS_RUNNING)
            return;
		ReturnToWorldMap.savePos = rigid.position;
		WorldMapTile currentTile = (WorldMapTile) tileref.GetTile(new Vector3Int(Mathf.FloorToInt(rigid.position.x), Mathf.FloorToInt(rigid.position.y), 0));
		CurrentTerrain = currentTile.terrainType;
		Debug.Log("Encounter!  " + encounterSteps + "," + CurrentTerrain);
				encounterSteps = 0f;
				
				
				WorldMapTile.TERRAIN_TYPE ter = currentTile.terrainType;
				
				DialogueTeleportActor[] MyEvent;
				MyEvent = new DialogueTeleportActor[1];
				MyEvent[0] = new DialogueTeleportActor();
				MyEvent[0].positionToMoveTo = Vector3.zero;
			
				MyEvent[0].trans = DialogueTeleportActor.TRANSITION_TYPE.SwordShield;
		
				
				if(ter == WorldMapTile.TERRAIN_TYPE.Grass)
					MyEvent[0].sceneToMoveTo = "BattleGrass";
				if(ter == WorldMapTile.TERRAIN_TYPE.Forest)
					MyEvent[0].sceneToMoveTo = "BattleForest";
				if(ter == WorldMapTile.TERRAIN_TYPE.Desert)
					MyEvent[0].sceneToMoveTo = "BattleDesert";
                if (ter == WorldMapTile.TERRAIN_TYPE.Snow)
                    MyEvent[0].sceneToMoveTo = "BattleSnow";
        FindObjectOfType<DialogueHandler>().StartEvent(MyEvent);
	}
	
	float healStep = 0f;
	void MovementWMap()
	{
		
		rigid.gravityScale = 0f;
		colliderWmap.enabled = true;
		colliderSideScroll.enabled = false;
		
		if(DialogueHandler.IS_RUNNING == true)
		{
			rigid.velocity = Vector2.zero;
			lastPos = rigid.position;
			healStep = 0f;
			encounterSteps = 0f;
			return;
		}
		if(lastPos != Vector2.zero)
		{
			float rel = Vector2.Distance(lastPos, rigid.position);
			if(rel > 0f)
			{
				healStep += rel;
				encounterSteps += rel;
			}
			
			if(healStep >= 1f)
			{
				healStep -= 1f;
				int tenPerc = GameData.GetMaxHP(GameDataHolder.instance.GetCurrentHero().hpUpgrades) / 20;
				gameObject.SendMessage("Damage", new DamageMessage(-tenPerc, DamageMessage.ELEMENT.Weapon));
				//Heal 10% every half step.
			}
			lastPos = rigid.position;
			
		}
		else //assume first step
			lastPos = rigid.position;
			
			
		if(tileref == null)
			tileref = GameObject.FindGameObjectWithTag("Terrain").GetComponent<Tilemap>();
		
		
		if(encounterSteps >= STEPS_TO_ENCOUNTER)
		{
			
			if( EncounterRegions.CurrentRegion != null)
			{
				FindObjectOfType<CameraUI>().worldMapText = "";
		
						
				Invoke("ToBattle", Time.deltaTime);
			}
		}
		
		Vector2 vel = Vector2.zero;
		
		vel.x = Input.GetAxis("Horizontal") * WMAP_VELOCITY;
		vel.y = Input.GetAxis("Vertical") * WMAP_VELOCITY;
		
		float difX = Screen.width * 0.1f;
		float difY = Screen.height * 0.1f;
		
		float centerX = Screen.width / 2f;
		float centerY = Screen.height / 2f;
		
		if(Input.GetMouseButton(0) == true)
		{
			if(Input.mousePosition.x < (centerX - difX))
				vel.x = -WMAP_VELOCITY;
			if(Input.mousePosition.x > (centerX + difX))
				vel.x = WMAP_VELOCITY;
			if(Input.mousePosition.y < (centerY - difY))
				vel.y = -WMAP_VELOCITY;
			if(Input.mousePosition.y > (centerY + difY))
				vel.y = WMAP_VELOCITY;
		}
		
		
		rigid.velocity = vel;
		
		
		anim.SetBool("Walking", rigid.velocity != Vector2.zero);
		if(vel.x > 0f)
			rend.flipX = true;
		if(vel.x < 0f)
			rend.flipX = false;
	}
	
	
	float jumpTimer = 0f;
	bool endJump = false;
	void MovementSide()
	{
		if (DialogueHandler.IS_RUNNING)
			return;
		colliderWmap.enabled = false;
		colliderSideScroll.enabled = true;
		encounterSteps = 0f;
		lastPos = Vector2.zero;
		rigid.gravityScale = 6f;
		float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
		if(horizontal < -0.6f)
			horizontal = -1f;
		if(horizontal > 0.6f)
			horizontal = 1f;
		if(horizontal >= -0.6f && horizontal <= 0.6f)
			horizontal = 0f;
		/*
		if(Application.isMobilePlatform)
		{
			horizontal = 0f;
			float ang = Input.acceleration.z;
			if(ang > 0.25f)
				horizontal = 1f;
			if(ang < -0.25f)
				horizontal = -1f;
			
			//Add based on axis
		}
		*/
		
		bool isGrounded = Physics2D.Raycast(rigid.position, Vector2.down, 0.2f, LayerMask.GetMask("Default", "Enemy"));
		
		if(Mathf.Abs(horizontal) > 0.2f)
		{
			anim.SetBool("Walking", true);
			if(Mathf.Abs(rigid.velocity.x) < (MAX_MOVEMENT * moveSpeedScale))
				rigid.AddForce(Vector2.right * moveSpeedScale * MOVE_SPEED * horizontal);
			
			if(isGrounded)
			{
				if(horizontal > 0f)
					rend.flipX = true;
				else
					rend.flipX = false;
			}
		}
		else
		{
			anim.SetBool("Walking", false);
		}
		
		
		if(isGrounded)
			endJump = false;
		if(CrossPlatformInputManager.GetAxis("Jump") == 0f && endJump == false)
		{
			endJump = true;
			//Interrupt jump
			if(rigid.velocity.y > 0f && isGrounded == false)
			{
				Vector2 v = rigid.velocity;
				v.y = 0f;
				rigid.velocity = v;
			}
		}
		
		if(jumpTimer > 0f)
			jumpTimer -= Time.fixedDeltaTime;
		if(CrossPlatformInputManager.GetAxis("Jump") != 0f && jumpTimer <= 0f)
		{
			if(isGrounded)
			{
				
				if(jumpFX != null)
					AudioSource.PlayClipAtPoint(jumpFX, Camera.main.transform.position);
				jumpTimer = 0.1f;
				//Floor found
				Vector2 v1 = rigid.velocity;
				v1.y = 0f;
				rigid.velocity = v1;
				rigid.AddForce(Vector2.up * JUMP_FORCE);
			}
		}
	}
}
