﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraMovement : MonoBehaviour
{
	private const float CAMERA_Z = -10;
	private const float MOVE_SPEED = 15f;
	private Transform player;
	private Camera cam;
	public static Vector3 targetPos;
	private float firstRun = 0.15f;
	
	public static bool isWorldMap = false;
	public GameObject beckySprite, johnnySprite;
	
    // Start is called before the first frame update
    void OnEnable()
    {
        SceneManager.activeSceneChanged += SceneChanged;
		CheckIsWorldMap();
		UpdateSpriteActive();
    }
	
	void OnDestroy()
	{
		SceneManager.activeSceneChanged -= SceneChanged;
	}
	
	void SceneChanged(Scene s1, Scene s2)
	{
		CheckIsWorldMap();
		firstRun = 0.35f;
		
		UpdateSpriteActive();
	}
	
	void UpdateSpriteActive()
	{
		if(GameDataHolder.instance.currentHero == 0)
		{
			beckySprite.SetActive(true);
			johnnySprite.SetActive(false);
		}
		if(GameDataHolder.instance.currentHero == 1)
		{
			beckySprite.SetActive(false);
			johnnySprite.SetActive(true);
		}
		
		if(GameDataHolder.instance.currentHero == 2)
		{
			beckySprite.SetActive(true);
			johnnySprite.SetActive(GameDataHolder.instance.johnnyStats.currentScene == SceneManager.GetActiveScene().name);
		}
		if(GameDataHolder.instance.currentHero == 3)
		{
			beckySprite.SetActive(GameDataHolder.instance.beckyStats.currentScene == SceneManager.GetActiveScene().name);
			johnnySprite.SetActive(true);
		}
	}
	
	void CheckIsWorldMap()
	{
		isWorldMap = false;
		if(SceneManager.GetActiveScene().name == "WorldMap")
			isWorldMap = true;
	}

    // Update is called once per frame
    void Update()
    {
		if(cam == null)
			cam = GetComponent<Camera>();
		if(player == null)
			player = GameObject.FindWithTag("Player").transform;
		
		if(player.gameObject.activeSelf == false)
			player = null;
        if(isWorldMap)
			CameraWMap();
		else
			CameraSideScroller();
		
    }
	
	void CameraWMap()
	{
		if(player != null)
		{
			Vector3 v = player.position;
			v.y -= 4f;
			v.z = CAMERA_Z;
		
			cam.transform.position = v;
		
			cam.transform.eulerAngles = new Vector3(-25f, 0f, 0f);
		}
	}
	
	void CameraSideScroller()
	{
		Vector3 topLeft = cam.ViewportToWorldPoint(new Vector3(1f, 0f, CAMERA_Z));
		Vector3 bottomRight = cam.ViewportToWorldPoint(new Vector3(0f, 1f, CAMERA_Z));
		float width = bottomRight.x - topLeft.x;
		float height = topLeft.y - bottomRight.y;
		
		cam.transform.eulerAngles = new Vector3(0f, 0f, 0f);
		
		
		targetPos = player.position;
		targetPos.z = CAMERA_Z;
		
		float left = targetPos.x - (width / 2f);
		float right = targetPos.x + (width / 2f);
		float top = targetPos.y + (height / 2f);
		float bottom = targetPos.y - (height / 2f);
		
		
		if(left < TileBounds.topLeft.x)
			targetPos.x = TileBounds.topLeft.x + (width / 2f);
		if(right > TileBounds.bottomRight.x)
			targetPos.x = TileBounds.bottomRight.x - (width / 2f);
		if(top > TileBounds.topLeft.y)
			targetPos.y = TileBounds.topLeft.y - (height / 2f);
		if(bottom < TileBounds.bottomRight.y)
			targetPos.y = TileBounds.bottomRight.y + (height / 2f);
		
		//Debug.Log("Cam TL<color=blue>" + topLeft + "</color> vs TB<color=blue>" + TileBounds.topLeft + "</color> Cam BR<color=green>" + bottomRight +  "</color> vs TB<color=green>" + TileBounds.bottomRight + "</color> new pos" + targetPos);

		if(firstRun >= 0f)
		{
			cam.transform.position = targetPos;
			firstRun -= Time.deltaTime;
			return;
		}

		if(DialogueHandler.IS_RUNNING == false)
        { //Don't move the camera while the dialogue is running.
            Vector3 newPos = Vector3.MoveTowards(cam.transform.position, targetPos, MOVE_SPEED * Time.deltaTime);
			
				
			
            cam.transform.position = newPos;
        }
		else
		{
			//Dialogue is running but it's teleporting to another scene
			if(DialogueTeleportActor.IsTeleporting == true)
				cam.transform.position = targetPos;
		}
		
	}
}
