﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerHealth : MonoBehaviour
{

	public static int minHealth, minMagic;
	public static float healthPerc, magicPerc;
	
	public SpriteRenderer rend;
	private float damageTimer = 0f;
	private const float DAMAGE_DELAY = 1f, SECONDS_PER_MP = 5f;

    private float playerMPRestoreTimer = 0f;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void LoadFromData()
	{

		
	}

	IEnumerator FlashRoutine(Color c)
	{
		rend.color = c;
		yield return new WaitForSeconds(0.1f);
		rend.color = Color.white;
	}
    public static bool DamageError = false;
    /// <summary>
    /// Should be called by GameObject.SendMessage.
    /// </summary>
    /// <param name="msg"></param>
	public void Damage(DamageMessage msg)
	{
        int amt = msg.Amount;  //We might have to modify this later.

        DamageError = false;
        int hp = GameDataHolder.instance.GetCurrentHero().minHP;
        int maxHp = GameData.GetMaxHP(GameDataHolder.instance.GetCurrentHero().hpUpgrades);
        if (amt > 0 && damageTimer > 0f)
        {
			StartCoroutine(FlashRoutine(Color.red));
            DamageError = true;
            return;
        }
        if (amt < 0f && Mathf.Approximately(hp, maxHp))
        {
			StartCoroutine(FlashRoutine(Color.green));
            DamageError = true;
            return;
        }
            
		
		damageTimer = DAMAGE_DELAY;

        if(amt > 0)
        {
            GameObject txt = GameObjectPool.GetInstance("DMG", transform.position, Quaternion.identity);
			if(txt != null)
				txt.SendMessage("ShowText", ("<color=red>-" + amt + " HP"));
        }
        

        hp -= amt;
        if (hp > maxHp)
            hp = maxHp;
		GameDataHolder.instance.GetCurrentHero().minHP = hp;
		
		if(hp <= 0)
			gameObject.SendMessage("OnDeath");
	}
	
	//Returns true if mp can be subtracted)
	public bool OffsetMagic(int amount)
	{
		int mp = GameDataHolder.instance.GetCurrentHero().minMP;
        int maxMP = GameData.GetMaxMP(GameDataHolder.instance.GetCurrentHero().mpUpgrades);
        if (amount > 0 && Mathf.Approximately(mp, maxMP))  //Don't restore MP if you don't have enough magic!
			return false;
        if (amount < 0 && mp < -amount)   //Don't have enough MP to do this
            return false;
        mp += amount;
		
		if(mp > maxMP)
			mp = maxMP;
		if(mp < 0)
            mp = 0;
		GameDataHolder.instance.GetCurrentHero().minMP = mp;
		
		
		return true;
		
	}

    // Update is called once per frame
    void Update()
    {
        if(damageTimer > 0f)
			damageTimer -= Time.deltaTime;
        

		GameData.Hero h = GameDataHolder.instance.GetCurrentHero();
		minHealth = h.minHP;
		minMagic = h.minMP;
        int maxMP = GameData.GetMaxMP(h.mpUpgrades);

        healthPerc = (h.minHP * 100 / GameData.GetMaxHP(h.hpUpgrades)) / 100f;
		magicPerc = (h.minMP * 100 / maxMP) / 100f;

        playerMPRestoreTimer += Time.deltaTime;
        if (playerMPRestoreTimer >= SECONDS_PER_MP)
        {
            playerMPRestoreTimer -= SECONDS_PER_MP;
            OffsetMagic(maxMP / 20);  //Add 5% of MP
        }

    }
}
