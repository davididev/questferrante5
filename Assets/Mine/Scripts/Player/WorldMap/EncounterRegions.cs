﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterRegions : MonoBehaviour
{
	[System.Serializable]
	public class EncounterHolder
	{
		public int[] enemiesInEncounter;
		
	}
	
	[SerializeField] public EncounterHolder[] possibleEncounters;
	
	public static EncounterHolder[] CurrentRegion;
	
	
	void OnTriggerEnter2D(Collider2D col)
	{
		CurrentRegion = possibleEncounters;
	}
	
	public static int[] GetRandomEncounter()
	{
		int max = CurrentRegion.Length - 1;
		int id = Random.Range(0, max);
		return CurrentRegion[id].enemiesInEncounter;
	}
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
