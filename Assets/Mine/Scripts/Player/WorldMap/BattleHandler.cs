﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleHandler : MonoBehaviour
{
	public GameObject[] enemyDatabase;
	
	
    // Start is called before the first frame update
    void OnEnable()
    {
        int[] enemies = EncounterRegions.GetRandomEncounter();
		
		for(int i = 0; i < enemies.Length; i++)
		{
			Debug.Log("Encounter!"  + enemies[i]);
			if(enemies[i] != -1)
			{
				int enemID = enemies[i];
				GameObject enemy = GameObject.Instantiate(enemyDatabase[enemID], transform.GetChild(i).position, Quaternion.identity) as GameObject;
			}
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
