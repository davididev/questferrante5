﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportRegion : MonoBehaviour
{
	
	public static DialogueTeleportActor[] MyEvent;
	[SerializeField] public DialogueTeleportActor myevent;
	public string areaText = "Test Forest";
	public int lockID = -1;
	
	public static int LockID = -1;
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(PlayerMovement.LockScreen)
			return;
		if(DialogueHandler.IS_RUNNING)
			return;
		if(col.tag != "Player")
			return;
		bool locked = false;
		if(lockID >= 0 && GameDataHolder.instance.locks[lockID] == '0')
			locked = true;
		
		LockID = lockID;
		if(locked)
		{
			FindObjectOfType<CameraUI>().worldMapText = "LOCKED";
		}
		else
		{
			FindObjectOfType<CameraUI>().worldMapText = areaText;
		}
		MyEvent = new DialogueTeleportActor[1];
		MyEvent[0] = myevent;
	}
	
	void OnTriggerExit2D(Collider2D col)
	{
		if(col.tag != "Player")
			return;
		FindObjectOfType<CameraUI>().worldMapText = "";
		LockID = -1;
	}
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
