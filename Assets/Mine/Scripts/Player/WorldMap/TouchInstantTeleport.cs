﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInstantTeleport : MonoBehaviour
{
    [SerializeField] public DialogueTeleportActor[] teleportCommand;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (DialogueHandler.IS_RUNNING)
            return;
		if(PlayerMovement.LockScreen)
			return;
        if(collision.tag == "Player")
        {
            FindObjectOfType<DialogueHandler>().StartEvent(teleportCommand);
        }
    }
}
