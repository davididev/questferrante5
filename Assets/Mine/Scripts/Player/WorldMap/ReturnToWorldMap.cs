﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToWorldMap : MonoBehaviour
{
	public Vector3 position;
	public static Vector3 savePos;
	public bool useSavePos = false;
	
	private DialogueTeleportActor[] MyEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(DialogueHandler.IS_RUNNING)
			return;
		if(col.tag == "Player")
		{
			if(useSavePos == true)
				position = savePos;
			
			
			DialogueTeleportActor[] MyEvent;
			MyEvent = new DialogueTeleportActor[1];
			MyEvent[0] = new DialogueTeleportActor();
			MyEvent[0].positionToMoveTo = position;
			MyEvent[0].sceneToMoveTo = "WorldMap";
			MyEvent[0].trans = DialogueTeleportActor.TRANSITION_TYPE.Fade;
		
			FindObjectOfType<DialogueHandler>().StartEvent(MyEvent);
		}
		
		//savePos = Vector3.zero;
	}
}
