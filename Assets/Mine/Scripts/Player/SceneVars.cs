﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneVars : MonoBehaviour
{
	public GameObject boilerplateConstant;
	public bool noWeapons = false;
    public AudioClip sceneMusic;
	public TextAsset dialogueOnMain, dialogueOnMainJohnny, dialogueOnMainBecky;
	public string keyNameVar = "";
	public static int keyVarID = -1;
	public static int keyCount = 0;
	public float GravityScale = 1f;
	
	public static bool NoWeapons;
	
	public GameObject[] enemyPoolPrefabs;
	
    // Start is called before the first frame update
    void Start()
    {
		Physics2D.gravity = new Vector2(0f, -3.26f * GravityScale);
        if(FindObjectOfType<BoilerRoot>() == null)
		{
			GameObject boiler = GameObject.Instantiate(boilerplateConstant, Vector3.zero, Quaternion.identity) as GameObject;
		}
		keyVarID = -1;
		if(keyNameVar != "")
		{
			keyVarID = VarsDB.instance.GetVarID(keyNameVar);
			keyCount = GameDataHolder.instance.gameVars[keyVarID];
		}
		NoWeapons = noWeapons;

        if (sceneMusic != null)
            Camera.main.GetComponent<PlayMusic>().PlaySong(sceneMusic);
		
		bool isBecky = true;
		if(GameDataHolder.instance.currentHero == 1 || GameDataHolder.instance.currentHero == 3)
			isBecky = false;
		
		bool dia = false;
		if(dialogueOnMain != null)
			dia = true;
		
		
		if(dia == false && dialogueOnMainBecky != null && isBecky)
			dia = true;
		if(dia == false && dialogueOnMainJohnny != null && !isBecky)
			dia = true;
		if(dia == true)
			StartCoroutine(StartDialogue());
		
		
		if(enemyPoolPrefabs != null)
		{
			if(enemyPoolPrefabs.Length > 0)
			{
				for(int i = 0; i < enemyPoolPrefabs.Length; i++)
				{
					GameObjectPool.InitPoolItem("Enemy" + i, enemyPoolPrefabs[i], 20);
				}
			}
		}
    }
	
	public void PlayMusic(AudioClip clip = null)
	{
		if (clip == null)
            Camera.main.GetComponent<PlayMusic>().PlaySong(sceneMusic);
		else
			Camera.main.GetComponent<PlayMusic>().PlaySong(clip);
	}

	IEnumerator StartDialogue()
	{
		yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
		while(DialogueHandler.IS_RUNNING == true)
		{
			yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
		}
		bool isBecky = true;
		if(GameDataHolder.instance.currentHero == 1 || GameDataHolder.instance.currentHero == 3)
			isBecky = false;
		
		bool characterSpecific = false;
		if(isBecky && dialogueOnMainBecky != null)
		{
			FindObjectOfType<DialogueHandler>().StartEvent(dialogueOnMainBecky);
			characterSpecific = true;
		}
		if(isBecky == false && dialogueOnMainJohnny != null)
		{
			FindObjectOfType<DialogueHandler>().StartEvent(dialogueOnMainJohnny);
			characterSpecific = true;
		}

		
		if(characterSpecific == false)  //There isn't a dialogue specific to this Johnny or Becky.
			FindObjectOfType<DialogueHandler>().StartEvent(dialogueOnMain);
	}

    // Update is called once per frame
    void Update()
    {
        if(keyVarID > -1)
			keyCount = GameDataHolder.instance.gameVars[keyVarID];
		else
			keyCount = 0;
    }
}
