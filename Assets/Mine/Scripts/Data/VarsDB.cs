﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VarsDB 
{
    private Dictionary<string, int> vars;  //string is var name, int is var ID
    public static VarsDB instance = new VarsDB();

	public string[] storyValues {private set; get;}

    public VarsDB()
    {
        vars = new Dictionary<string, int>();
        vars.Add("%story", 0);
		vars.Add("%keys_ArianTemple", 1);
		vars.Add("%worldMapTutorial", 2);
		vars.Add("%gotFerranteSupplies", 3);
        vars.Add("%keys_Goblin", 4);
		vars.Add("%waterMSG", 5);
		vars.Add("%dragon1_Kavu", 6);
		vars.Add("%dragon2_Kavu", 7);
		vars.Add("%keys_SelvaCave", 8);
		vars.Add("%keys_Aquarius", 9);

        storyValues = new string[] {"Well, well, well!",  //0
		"The burning forest", //1
		"Doll in the cave", //2
		"Becky leaves Ariel!", //3
		"King Ferrante departs!", //4
		"The Goblin's Castle", //5
		"The Frozen Land", //6
		"Obtained Key Of Mon!", //7
		"Kavu's Dragon Mountain", //8
		"Is Johnny in Selva?", //9
		"Where is my queen?", //10
		"My queen went south?", //11
		"Aquarius is saved!", //12
		"Into the jungle!", //13
        "Fighting the boss in the jungle!", //14
        "Johnny looks for Becky!", //15
        "Johnny fights the killer robot!", //16
        "Down with the necromancers!" //17
		};
    }

    /// <summary>
    /// Set the value in game data based on the variable name
    /// </summary>
    /// <param name="varName">Name of the variable in the database</param>
    /// <param name="value">Value to set it to.</param>
    public void SetVarValue(string varName, int valueY)
    {
        int x;
        if (vars.TryGetValue(varName, out x))
        {
            GameDataHolder.instance.gameVars[x] = valueY;
        }
    }


    /// <summary>
    /// Get the variable from game data based on the variable name
    /// </summary>
    /// <param name="id">The name of the variable in the database</param>
    /// <returns></returns>
    public int GetVarValue(string id)
    {
        int x;
        if (vars.TryGetValue(id, out x))
        {
            return GameDataHolder.instance.gameVars[x];
        }
        else
            return 0;
    }
	
	public int GetVarID(string id)
	{
		Dictionary<string, int>.Enumerator enum1 = vars.GetEnumerator();
		while(enum1.MoveNext())
		{
			if(enum1.Current.Key == id)
			{
				return enum1.Current.Value;
			}
		}
		return -1;
	}

    public string[] GetAllVarNames()
    {
        int x = 0;
        string[] ret = new string[vars.Count];
        Dictionary<string, int>.Enumerator enum1 = vars.GetEnumerator();
        while(enum1.MoveNext())
        {
            ret[x] = enum1.Current.Key;
			x++;
        }

        return ret;

    }
}
