﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDB 
{
	public static ItemDB instance = new ItemDB();
	public class Dat
	{
		public bool reusable;
		public string nameOfItem;
		public string description;
		public int requiredMP;
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="r">Reusable</param>
        /// <param name="n">Name of item</param>
        /// <param name="d">Description of item</param>
        /// <param name="m">MP needed</param>
		public Dat(bool r, string n, string d, int m = 0)
		{
			reusable = r;
			nameOfItem = n;
			description = d;
			requiredMP = m;
		}
	}
    private Dictionary<char, Dat> datam = new Dictionary<char, Dat>();
	
	
	public ItemDB()
	{
		datam.Add('0', new Dat(true, "---", "Empty hand"));
		datam.Add('A', new Dat(true, "Royal Sword", "A sword owned by the royal guard"));
		datam.Add('B', new Dat(true, "Morning Star", "Attacks in an arc- good for lower enemies."));
		datam.Add('C', new Dat(true, "Boomarang", "Used by the warrior who likes to keep distance."));
		datam.Add('D', new Dat(true, "Angel Axe", "A massive weapon for warriors who like to keep distance."));
		datam.Add('E', new Dat(true, "Drain Sabre", "A sword short for stabbing.  Each hit restores your HP."));
		datam.Add('F', new Dat(true, "Cross Clobber", "A boomarang with angelic properties."));
		datam.Add('1', new Dat(false, "Health Potion", "Restores 200 HP"));
		datam.Add('2', new Dat(false, "Magic Potion", "Restores 30 MP"));
		datam.Add('3', new Dat(false, "Hi-Health Potion", "Restores 1000 HP"));
		datam.Add('4', new Dat(false, "Hi-Magic Potion", "Restores 120 MP"));
		datam.Add('5', new Dat(false, "Throwing Knife", "Throw a knife at an enemy"));
		datam.Add('6', new Dat(false, "Ninja Star", "Throw a shiruken at an enemy."));
		datam.Add('7', new Dat(false, "Throwing Axe", "The ultimate throwing weapon."));
		datam.Add('8', new Dat(false, "Elixer", "Restores all HP and MP."));
		datam.Add('a', new Dat(true, "Water Ball", "(2 MP) Create a wave of water and launch it forward.", 2));
		datam.Add('b', new Dat(true, "Fireball", "(2 MP) Create a fireball and launch it forward..", 2));
		datam.Add('c', new Dat(true, "Water Burst", "(5 MP) Throw a water ball in all directions.", 5));
		datam.Add('d', new Dat(true, "Fire Burst", "(5 MP) Throw fireball in all directions.", 5));
		datam.Add('e', new Dat(true, "Wind Blast", "(5 MP) Shoot whirlwinds around you.", 5));
		datam.Add('f', new Dat(true, "Quake", "(5 MP) Create a pulse of energy around you.", 5));
		datam.Add('g', new Dat(true, "Tornado", "(7 MP) Fire a tarnoda that turns into a wind blast.", 7));
		datam.Add('h', new Dat(true, "Earth Ray", "(7 MP) Fire a ray that turns into a tornado.", 7));
	}
	
	
	public string GetItemName(char id)
	{
		return GetItem(id).nameOfItem;
	}
	
	public string GetItemDesc(char id)
	{
		return GetItem(id).description;
	}
	
	public bool GetItemReusable(char id)
	{
		return GetItem(id).reusable;
	}
	
	public int GetItemMP(char id)
	{
		return GetItem(id).requiredMP;
	}

	
	public Dat GetItem(char id)
	{
		Dat i;
		if(datam.TryGetValue(id, out i) == true)
		{
			return i;
		}
		else
			return null;
	}
	
}
