﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData 
{
	[System.Serializable]
	public class Hero
	{
		[SerializeField] public int XP = 0;
		[SerializeField] public int level = 0;
		[SerializeField] public int hpUpgrades = 0;
		[SerializeField] public int mpUpgrades = 0;
		[SerializeField] public int minHP = 100;
		[SerializeField] public int minMP = 20;
		[SerializeField] public int gold = 20;
		[SerializeField] public int[] handItems;
		[SerializeField] public string currentScene = "0";
		[SerializeField] public Vector2 currentPosition;
		[SerializeField] public Item[] items;
		
		public int GetDamageMultiplier()
		{
			int lvl = (level + 1)  * 2 / 3;
			if(lvl < 1)
				lvl = 1;
			return lvl;
		}
		
		public Hero()
		{
			items = new Item[99];
			for(int i = 0; i < items.Length; i++)
			{
				items[i].id = '0';
				items[i].count = 0;
			}
			
			items[0].id = 'A';  //Left hand is hero sword by default.
			items[0].count = 1;
			
			
			handItems = new int[2];
			for(int i = 0; i < handItems.Length; i++)
			{
				handItems[i] = -1;
			}
			handItems[0] = 0;
		}
	}
	
	[System.Serializable]
	public struct Item
	{
		[SerializeField] public char id;
		[SerializeField] public int count;
	}
	
	private static int lastHPUpgrades = -1, lastHP = 0, lastMPUpgrades = -1, lastMP = 0;
	public static int GetMaxHP(int hpUpgrades)
	{
		if(lastHPUpgrades == hpUpgrades)
			return lastHP;
		
		lastHPUpgrades = hpUpgrades;
		
		int h = 100;
		for(int i = 0; i < hpUpgrades; i++)
		{
			h = h * 108 / 100;
		}
		lastHP = h;
		lastHPUpgrades = hpUpgrades;
		return h;
	}
	
	public static int GetMaxMP(int mpUpgrades)
	{
		if(lastMPUpgrades == mpUpgrades)
			return lastMP;
		int m = 20;
		for(int i = 0; i < mpUpgrades; i++)
		{
			m = m * 112 / 100;
		}
		lastMP = m;
		lastMPUpgrades = mpUpgrades;
		return m;
	}

    [SerializeField] public Hero beckyStats;
    [SerializeField] public Hero johnnyStats;
	[SerializeField] public int currentHero = 0;  //0 for just Becky, 1 for just Johnny, 2 for Becky (swappable with 3), 3 for Johnny (swap with 2)
    [SerializeField] public int[] gameVars = new int[100];
	int zero = 0;
	[SerializeField] public string healthMagicUpgrades = "";
	[SerializeField] public string chests = "";
	[SerializeField] public string locks = "";
	[SerializeField] public string keys = "";
	
	public Hero GetCurrentHero()
	{
		if(currentHero == 0 || currentHero == 2)
			return beckyStats;
		else
			return johnnyStats;
	}

    /// <summary>
    /// Get item ID if it exists
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public int ItemExists(char ID)
    {
        bool alreadyexists = false;
        Hero h = GetCurrentHero();
        int i = -1;
        for (i = 0; i < h.items.Length; i++)
        {
            if (h.items[i].id == ID)
            {
                Debug.Log("Item exists as " + i);
                alreadyexists = true;
                break;
            }
        }
        if (alreadyexists == false)
            i = -1;
        return i;
    }

	public void AddItem(char ID, int c)
	{
		Debug.Log("Adding item " + ID);
		//Hero h = GetCurrentHero();

        int itemID = ItemExists(ID);
		if(itemID >= 0)
        {
            GetCurrentHero().items[itemID].count += c;
            if(GetCurrentHero().items[itemID].count <= 0)
            {
                GetCurrentHero().items[itemID].id = '0';
                GetCurrentHero().items[itemID].count = 0;
            }
        }
                    
        
		if(itemID == -1)  //Doesn't exist yet!
		{
			for(int i = 0; i < GetCurrentHero().items.Length; i++)
			{
				if(GetCurrentHero().items[i].id == '0')
				{
					Debug.Log("Item does not exist.  Adding it as " + i);
                    GetCurrentHero().items[i].count = c;
                    GetCurrentHero().items[i].id = ID;
					break;
				}
			}
		}
	}
	
    public GameData()
	{
		//New file
		
		beckyStats = new Hero();
		johnnyStats = new Hero();
		currentHero = 0;
		for(int i = 0; i < gameVars.Length; i++)
		{
			gameVars[i] = 0;
		}
		
		GetCurrentHero().currentScene = "Arian_House1";
		GetCurrentHero().currentPosition = new Vector2(0f, 0f);
		chests = new string('0', 100);
		healthMagicUpgrades = new string('0', 100);
		locks = new string('0', 100);
		keys = new string('0', 100);
	}
}
