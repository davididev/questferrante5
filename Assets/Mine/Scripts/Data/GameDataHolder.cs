﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameDataHolder : MonoBehaviour
{
    public static GameData instance = new GameData();
    public static int fileID = 0;

    public static void NewFile()
    {
        instance = new GameData();
    }

    public static void LoadFile(int fileID)
    {
        string fileName = Application.persistentDataPath + "/file" + fileID + ".dat";
        StreamReader read = new StreamReader(fileName);
        instance = (GameData) JsonUtility.FromJson(read.ReadToEnd(), typeof(GameData));
        read.Close();
    }

    public static void SaveFile(int fileID)
    {
        string f = JsonUtility.ToJson(instance);
        string fileName = Application.persistentDataPath + "/file" + fileID + ".dat";
        StreamWriter write = new StreamWriter(fileName);
        write.Write(f);
        write.Close();

    }
	
	public static void DeleteFile(int fileID)
	{
		string fileName = Application.persistentDataPath + "/file" + fileID + ".dat";
		File.Delete(fileName);
	}
}
