﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemyHealth))]
public class EnemyHealthEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EnemyHealth enem = (EnemyHealth)target;
        GUILayout.Label("----Multipliers----");
        string[] elements = System.Enum.GetNames(typeof(DamageMessage.ELEMENT));
        for(int i = 0; i < elements.Length - 1; i++)
        {
            string s = enem.multipliers[i].ToString();

            GUILayout.BeginHorizontal();
            GUILayout.Label(elements[i]);
            s = GUILayout.TextField(s, 5);
            GUILayout.EndHorizontal();
            int x;


            if (int.TryParse(s, out x) == true)
                enem.multipliers[i] = x;
            else
                enem.multipliers[i] = 100;
        }
    }
}
