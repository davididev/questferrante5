﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

public class VarsWindow : EditorWindow
{
    
	
	public int healthMagic, chests, locks, keys;
	string path = "";
	
	[MenuItem("Window/DD/VarsWindow")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(VarsWindow));
    }
	
	int DrawField(string label, int v)
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(label);
		string s1 = v.ToString();
        s1 = EditorGUILayout.TextField(s1);
		int i;
		if(int.TryParse(s1, out i) == true)
		{
			v = i;
		}
		
		if(GUILayout.Button("+"))
		{
			v++;
		}
		
		EditorGUILayout.EndHorizontal();
		return v;
	}
	
	void OnGUI()
	{
		if(path == "")
		{
			path = "Assets/Resources/notouchie.txt";
			AssetDatabase.ImportAsset(path);
			TextAsset asset = Resources.Load("notouchie") as TextAsset;
		
			string[] args = asset.text.Split(',');
			healthMagic = int.Parse(args[0]);
			chests = int.Parse(args[1]);
			locks = int.Parse(args[2]);
			keys = int.Parse(args[3]);
		
		}
		healthMagic = DrawField("HP/MP", healthMagic);
		chests = DrawField("Chests", chests);
		locks = DrawField("Locks", locks);
		keys = DrawField("Keys", keys);		
		
		if(GUILayout.Button("Save"))
		{
			string s = healthMagic + "," + chests + "," + locks + "," + keys;
			File.WriteAllBytes(path, Encoding.ASCII.GetBytes(s));
		}
	}
}
