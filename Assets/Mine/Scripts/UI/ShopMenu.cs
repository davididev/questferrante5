﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public struct ItemToBuy
{
	public char itemID;
	public int itemCount;
	public int price;
}

public class ShopMenu : MonoBehaviour
{
	public static ItemToBuy[] ItemsInShop;
	public static bool Active = false;
	public GameObject[] buttonRoots;
	public GameObject confirmBuyButton, cancelBuyButton;
	public TMPro.TextMeshProUGUI[] itemLabels;
	public TMPro.TextMeshProUGUI itemDescription;
	public AudioClip boughtItemSnd;
	
    // Start is called before the first frame update
    void OnEnable()
    {
		Active = true;
        RefreshItems();
    }
	
	void OnDisable()
	{
		confirmBuyButton.SetActive(false);
		cancelBuyButton.SetActive(false);
		itemDescription.text = "";
		Active = false;
	}
	
	public void Close()
	{
		gameObject.SetActive(false);
	}

	//This function should only show desc now.
	public void BuyItem(int id)
	{
		SelectedID = id;
		char c = ItemsInShop[id].itemID;
		itemDescription.text = "<color=yellow>" + ItemDB.instance.GetItemName(c) + " x" + ItemsInShop[id].itemCount + " (" + ItemsInShop[id].price + " g)</color>\n" + ItemDB.instance.GetItemDesc(c);
		
		
		confirmBuyButton.SetActive(true);
		cancelBuyButton.SetActive(true);
		EventSystem.current.SetSelectedGameObject(confirmBuyButton);
	}
	
	public static int SelectedID = 0;
	
	public void ConfirmBuy()
	{
		int id = SelectedID;
		int myPrice = ItemsInShop[id].price;
		int myGold = GameDataHolder.instance.GetCurrentHero().gold;
		if(myGold >= myPrice)
		{
			GameDataHolder.instance.GetCurrentHero().gold -= myPrice;
			GameDataHolder.instance.AddItem(ItemsInShop[id].itemID, ItemsInShop[id].itemCount);
			
			if(boughtItemSnd != null)
				AudioSource.PlayClipAtPoint(boughtItemSnd, Camera.main.transform.position);
			
			itemLabels[id].text = "Bought one!";
			
		}
		else
			itemLabels[id].text = "Not enough gold!";
		
		
		StartCoroutine(RefreshDaItem());
	}
	
	public void CancelBuy()
	{
		EventSystem.current.SetSelectedGameObject(buttonRoots[SelectedID]);
		cancelBuyButton.SetActive(false);
		confirmBuyButton.SetActive(false);
		itemDescription.text = "";
	}
	
	IEnumerator RefreshDaItem()
	{
		yield return new WaitForSecondsNoTimeScale(2f);
		RefreshItems();
	}
	
	void RefreshItems()
	{
		for(int i = 0; i < buttonRoots.Length; i++)
		{
			if(i < ItemsInShop.Length)
			{
				buttonRoots[i].SetActive(true);
				string thisLabel = ItemDB.instance.GetItemName(ItemsInShop[i].itemID);
				itemLabels[i].text = thisLabel + " x " + ItemsInShop[i].itemCount + ": $" + ItemsInShop[i].price;
			}
			else
			{
				buttonRoots[i].SetActive(false);
			}
		}
	}
	
	
	

    // Update is called once per frame
    void Update()
    {
        
    }
}
