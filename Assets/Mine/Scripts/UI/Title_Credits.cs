﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_Credits : MonoBehaviour
{
	public TextAsset creditsTxt;
	public TMPro.TextMeshProUGUI creditsContent;
	
	private string[] divides;
	
	int pageID = 0;
	
    // Start is called before the first frame update
    void OnEnable()
    {
		string s = creditsTxt.text;
        divides = s.Split('*');
		pageID = 0;
		SetPageID();
    }
	
	public void OffsetPage(int off)
	{
		pageID += off;
		SetPageID();
	}
	
	void SetPageID()
	{
		if(pageID < 0)
			pageID = divides.Length - 1;
		if(pageID >= divides.Length)
			pageID = 0;
		
		
		creditsContent.text = divides[pageID];
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
