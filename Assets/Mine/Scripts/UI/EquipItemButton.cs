﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipItemButton : MonoBehaviour
{
	public char itemID;
	public TMPro.TextMeshProUGUI label;
	private Pause_Equip root;
    // Start is called before the first frame update
    void Start()
    {
        
    }
	
	public void SetRoot(Pause_Equip r)
	{
		root = r;
	}
	
	public void OnHover()
	{
		root.description.text = ItemDB.instance.GetItemDesc(itemID);
	}
	

    // Update is called once per frame
    void Update()
    {
        
    }
}
