﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        Time.timeScale = 1f / 360f;
        PauseMenu.lockPauseMenu = true;
        transform.localScale = new Vector3(1f, 0.01f, 1f);
        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "ignoretimescale", true, "time", 1f));
    }

    public void Upgrade(int id)
    {
        Time.timeScale = 1f;
        PauseMenu.lockPauseMenu = false;
        if (id == 0) //Just HP
            GameDataHolder.instance.GetCurrentHero().hpUpgrades += 2;
        if (id == 1) //Just MP
            GameDataHolder.instance.GetCurrentHero().mpUpgrades += 2;
        if (id == 2) //Both HP and MP
        {
            GameDataHolder.instance.GetCurrentHero().hpUpgrades += 1;
            GameDataHolder.instance.GetCurrentHero().mpUpgrades += 1;
        }
		GameDataHolder.instance.GetCurrentHero().minHP = GameData.GetMaxHP(GameDataHolder.instance.GetCurrentHero().hpUpgrades);
		GameDataHolder.instance.GetCurrentHero().minMP = GameData.GetMaxMP(GameDataHolder.instance.GetCurrentHero().mpUpgrades);

		GameDataHolder.instance.GetCurrentHero().XP -= 100;
		GameDataHolder.instance.GetCurrentHero().level++;

        if(GameDataHolder.instance.GetCurrentHero().XP < 100)
			StartCoroutine(Close());
    }

    IEnumerator Close()
    {
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(1f, 0.01f, 1f), "ignoretimescale", true, "time", 1f));
        yield return new WaitForSecondsNoTimeScale(1f);
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
