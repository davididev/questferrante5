﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DefaultButton : MonoBehaviour {

    private void OnEnable()
    {
        StartCoroutine(MoveItOrLoseIt());
        
    }

    IEnumerator MoveItOrLoseIt()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
	
}
