﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Pause_Equip : MonoBehaviour
{
	public GameObject buttonTemplete;
	private Button[] itemList;
	public Transform equipmentContent;
	public GameObject equipmentHolder;
	public Button leftHandButton, rightHandButton;
	
	public TMPro.TextMeshProUGUI description;
	
    // Start is called before the first frame update
    void OnEnable()
    {
        UpdateHandText();
		leftHandButton.interactable = true;
		rightHandButton.interactable = true;
		if(equipmentContent.childCount == 1)
		{
			itemList = new Button[99];
			itemList[0] = buttonTemplete.GetComponent<Button>();
			itemList[0].onClick.AddListener(delegate{EquipItem(-1);});
			EquipItemButton text2 = itemList[0].GetComponent<EquipItemButton>();
			text2.SetRoot(this);
			text2.itemID = '0';
			text2.label.text = "---";
			
			for(int i = 1; i < itemList.Length; i++)
			{
				GameObject g = GameObject.Instantiate(buttonTemplete, buttonTemplete.transform.parent) as GameObject;
				itemList[i] = g.GetComponent<Button>();
				int x = i-1;
				itemList[i].onClick.AddListener(delegate{EquipItem(x);});
				
			}
			
			
			
		}
		UpdateItemText();
		
		
		equipmentHolder.SetActive(false);
    }
	
	
	
	void UpdateItemText()
	{
		for(int i = 1; i < itemList.Length; i++)
		{
			itemList[i].gameObject.SetActive(true);
			GameData.Item it = GameDataHolder.instance.GetCurrentHero().items[i-1];
			EquipItemButton text1 = itemList[i].GetComponentInChildren<EquipItemButton>();
			text1.itemID = it.id;
			text1.SetRoot(this);
			string item = ItemDB.instance.GetItemName(it.id);
			bool reuse = ItemDB.instance.GetItemReusable(it.id);
			if(it.id == '0')
				itemList[i].gameObject.SetActive(false);
			else
			{
				
				if(reuse)
					text1.label.text = item;
				else
					text1.label.text = item + " x" + it.count + "";
			}
		}
	}
	
	
	//When you close this panel
	void OnDisable()
	{
		UpdatePool();
	}
	
	void UpdateHandText()
	{
		TMPro.TextMeshProUGUI text1 = leftHandButton.GetComponentInChildren<TMPro.TextMeshProUGUI>();
		TMPro.TextMeshProUGUI text2 = rightHandButton.GetComponentInChildren<TMPro.TextMeshProUGUI>();
		
		int id1 = GameDataHolder.instance.GetCurrentHero().handItems[0];
		int id2 = GameDataHolder.instance.GetCurrentHero().handItems[1];
		Debug.Log("Item IDS: " + id1 + "," + id2);
		string s1 = "---", s2 = "---";
		if(id1 > -1)
		{
			if(!ItemDB.instance.GetItemReusable(GameDataHolder.instance.GetCurrentHero().items[id1].id))
				s1 = ItemDB.instance.GetItemName(GameDataHolder.instance.GetCurrentHero().items[id1].id) 
					+ " x" + GameDataHolder.instance.GetCurrentHero().items[id1].count;
			else
				s1 = ItemDB.instance.GetItemName(GameDataHolder.instance.GetCurrentHero().items[id1].id);
		}
		if(id2 > -1)
		{
			if(!ItemDB.instance.GetItemReusable(GameDataHolder.instance.GetCurrentHero().items[id2].id))
				s2 = ItemDB.instance.GetItemName(GameDataHolder.instance.GetCurrentHero().items[id2].id) 
					+ " x" + GameDataHolder.instance.GetCurrentHero().items[id2].count;
			else
				s2 = ItemDB.instance.GetItemName(GameDataHolder.instance.GetCurrentHero().items[id2].id);
		}
		text1.text = "Left Hand\n(" + s1 + ")";
		text2.text = "Right Hand\n(" + s2 + ")";
	}
	
	public void UpdatePool()
	{
		GameObject pm = GameObject.FindGameObjectWithTag("Player");
		if(pm != null)
		{
			Debug.Log("Init pool");
			pm.SendMessage("InitPoolForWeapons");
		}
	}
	
	private int selectedHandID;
	public void StartEquip(int handID)
	{
		selectedHandID = handID;
		leftHandButton.interactable = false;
		rightHandButton.interactable = false;
		equipmentHolder.SetActive(true);
		EventSystem.current.SetSelectedGameObject(itemList[0].gameObject);
	}
	
	public void EquipItem(int itemID)
	{
		leftHandButton.interactable = true;
		rightHandButton.interactable = true;
		equipmentHolder.SetActive(false);
		GameDataHolder.instance.GetCurrentHero().handItems[selectedHandID] = itemID;
		
		if(selectedHandID == 0)
			EventSystem.current.SetSelectedGameObject(leftHandButton.gameObject);
		else
			EventSystem.current.SetSelectedGameObject(rightHandButton.gameObject);
		
		int selectedItemID = GameDataHolder.instance.GetCurrentHero().handItems[selectedHandID];
		string itemStr = "0";
		if(selectedItemID > -1)
			itemStr = GameDataHolder.instance.GetCurrentHero().items[selectedItemID].id.ToString();
		
		description.text = ItemDB.instance.GetItemDesc(itemStr[0]);
		
		UpdateHandText();
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
