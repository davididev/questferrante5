﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TitleMenu : MonoBehaviour
{
	public GameObject fileMenuButton, creditsMenuButton;
	public GameObject fileMenu, creditsMenu, confirmDelete;
    // Start is called before the first frame update
    void Start()
    {
		Time.timeScale = 1f;
		BoilerRoot root = FindObjectOfType<BoilerRoot>();
		if(root != null)
			Destroy(root.gameObject);
        SetPanelID(0);
    }
	
	public void SetPanelID(int id)
	{
		fileMenu.SetActive(id == 0);
		creditsMenu.SetActive(id == 1);
		confirmDelete.SetActive(id == 2);
		
		if(id == 0)
			EventSystem.current.SetSelectedGameObject(fileMenuButton);
		if(id == 1)
			EventSystem.current.SetSelectedGameObject(creditsMenuButton);
	}
	
	public void Quit()
	{
		Application.Quit();
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
