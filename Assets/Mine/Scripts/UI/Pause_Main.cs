﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Pause_Main : MonoBehaviour
{
	public PauseMenu root;
	public Button saveButton;
	
    // Start is called before the first frame update
    void OnEnable()
    {
        saveButton.interactable = CameraMovement.isWorldMap;
		TMPro.TextMeshProUGUI txt = saveButton.GetComponentInChildren<TMPro.TextMeshProUGUI>();
		txt.text = "Save";
    }
	
	public void QuitGame()
	{
		SceneManager.LoadScene("Title");
	}
	
	public void SaveGame()
	{
		GameDataHolder.instance.GetCurrentHero().currentPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
		GameDataHolder.SaveFile(GameDataHolder.fileID);
		StartCoroutine(ShowSaved());
	}
	
	IEnumerator ShowSaved()
	{
		TMPro.TextMeshProUGUI txt = saveButton.GetComponentInChildren<TMPro.TextMeshProUGUI>();
		txt.text = "Game saved!";
		yield return new WaitForSecondsRealtime(1f);
		txt.text = "Save";
	}
	
	
	public void ReturnToGame()
	{
		root.gameObject.SetActive(false);
		Time.timeScale = 1f;
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
