﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CrossPlatformAnimator : MonoBehaviour
{
	private Animator anim;
	public string axisToCheck = "Fire1";
	private bool lastChecked = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		
        if(anim == null)
			anim = GetComponent<Animator>();
		
		if(axisToCheck.Contains("Fire"))
		{
			anim.SetBool("Disabled", SceneVars.NoWeapons);
		}
		
		if(CrossPlatformInputManager.GetAxis(axisToCheck) == 0f)
		{
			if(lastChecked == true)
			{
				anim.SetTrigger("TurnOff");
				lastChecked = false;
			}
		}	
		else
		{
			if(lastChecked == false)
			{
				anim.SetTrigger("TurnOn");
				lastChecked = true;
			}
		}
    }
}
