﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause_Stats : MonoBehaviour
{
	public GameObject beckyButton, johnnyButton;
	
	public TMPro.TextMeshProUGUI level, xp, strText, hpUpgrades, mpUpgrades, hp, mp, gold;
	
    // Start is called before the first frame update
    void OnEnable()
    {
        if(VarsDB.instance.GetVarValue("%story") >= 3)  //Only show the buttons if you've unlocked Johnny
		{
			beckyButton.SetActive(true);
			johnnyButton.SetActive(true);
		}
		else
		{
			beckyButton.SetActive(false);
			johnnyButton.SetActive(false);
		}
		
		if(GameDataHolder.instance.currentHero == 0 || GameDataHolder.instance.currentHero == 2)  //Becky
			ShowStats(0);
		else
			ShowStats(1);
    }
	
	public void ShowStats(int heroID)
	{
		GameData.Hero h = GameDataHolder.instance.beckyStats;
		if(heroID == 1)
			h = GameDataHolder.instance.johnnyStats;
		
		
		level.text = "Level: <color=yellow>" + (h.level+1) + "</color>";
		strText.text = "Power: <color=yellow>" + h.GetDamageMultiplier() + "</color>";
		xp.text = "XP: <color=yellow>" + h.XP + "/100</color>";
		hpUpgrades.text = "HP Upgrades: <color=yellow>" + h.hpUpgrades + "</color>";
		mpUpgrades.text = "MP Upgrades: <color=yellow>" + h.mpUpgrades + "</color>";
		hp.text = "HP: <color=yellow>" + h.minHP + "/" + GameData.GetMaxHP(h.hpUpgrades) + "</color>";
		mp.text = "MP: <color=yellow>" + h.minMP + "/" + GameData.GetMaxMP(h.mpUpgrades) + "</color>";
		gold.text = "Gold: <color=yellow>" + h.gold.ToString("D7") + "</color>";
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
