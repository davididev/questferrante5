﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAnimSoundFX : MonoBehaviour {

	[SerializeField] AudioClip hoverFX, pressFX, disableFX;
	public string highlightedAction = "";
	// Use this for initialization
	void Start () {
		
	}
	
	public void PlayHoverSound()
	{
		//if Timescale is 0, sound won't play >:
		float lastTimeScale = Time.timeScale;
		Time.timeScale = 1f;
		AudioSource.PlayClipAtPoint(hoverFX, Camera.main.transform.position);
		Time.timeScale = lastTimeScale;
		
		if(highlightedAction != "")
			gameObject.SendMessage(highlightedAction);
	}
	
	public void PlayPressSound()
	{
		//if Timescale is 0, sound won't play >:
		float lastTimeScale = Time.timeScale;
		Time.timeScale = 1f;
		AudioSource.PlayClipAtPoint(pressFX, Camera.main.transform.position);
		Time.timeScale = lastTimeScale;
	}
	
	public void PlayDisableSound()
	{
		//if Timescale is 0, sound won't play >:
		float lastTimeScale = Time.timeScale;
		Time.timeScale = 1f;
		AudioSource.PlayClipAtPoint(disableFX, Camera.main.transform.position);
		Time.timeScale = lastTimeScale;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
