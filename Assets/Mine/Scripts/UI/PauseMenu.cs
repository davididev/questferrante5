﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PauseMenu : MonoBehaviour
{
    public static bool lockPauseMenu = false;
	private bool firstPause = true;
	public GameObject mainPanel, equipPanel, statsPanel, confirmQuitPanel;
    // Start is called before the first frame update
    void OnEnable()
    {
		firstPause = false;
        SetPanelID(0);
    }
	
	public void SetPanelID(int id)
	{
		mainPanel.SetActive(id == 0);
		equipPanel.SetActive(id == 1);
		statsPanel.SetActive(id == 2);
		confirmQuitPanel.SetActive(id == 3);
	}

    // Update is called once per frame
    void Update()
    {
		//Debug.Log("Pause Axis: " + CrossPlatformInputManager.GetAxis("Pause") + " firstpause " + firstPause);
		if(Mathf.Approximately(CrossPlatformInputManager.GetAxis("Pause"), 0f))
		{
			firstPause = true;
		}
		if(CrossPlatformInputManager.GetAxis("Pause") != 0f && firstPause == true)
		{
			firstPause = false;
			if(Time.timeScale < 0.5f)
			{
				gameObject.SetActive(false);
				Time.timeScale = 1f;
				return;
			}
		}
    }
}
