﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Title_Files : MonoBehaviour
{
	[System.Serializable] public struct FilePiece
	{
		public TMPro.TextMeshProUGUI fileStory;
		public TMPro.TextMeshProUGUI fileID;
	}
	
	public GameObject QuadSpoilsText;
	public FilePiece[] filePieces;
	
	
    // Start is called before the first frame update
    void OnEnable()
    {
		QuadSpoilsText.SetActive(PlayerMovement.ExtraSpoils);
        for(int i = 0; i < filePieces.Length; i++)
		{
			filePieces[i].fileID.text = "File " + (i+1);
			try
			{
				GameDataHolder.LoadFile(i);
				int story = GameDataHolder.instance.gameVars[0];
				filePieces[i].fileStory.text = VarsDB.instance.storyValues[story];
			}
			catch(System.Exception e)
			{
				filePieces[i].fileStory.text = "Empty file";
			}
		}
    }
	
	public void PlayFile(int id)
	{
		try
		{
			GameDataHolder.LoadFile(id);
		}
		catch(System.Exception e)
		{
			GameDataHolder.NewFile();
			GameDataHolder.SaveFile(id);
		}
		GameDataHolder.fileID = id;
		
		SceneManager.LoadScene(GameDataHolder.instance.GetCurrentHero().currentScene);
		gameObject.SetActive(false);
	}
	
	public void DeleteFile(int id)
	{
		Title_ConfirmDelete.fileID = id;
		FindObjectOfType<TitleMenu>().SetPanelID(2);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
