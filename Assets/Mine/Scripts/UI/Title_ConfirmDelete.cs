﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title_ConfirmDelete : MonoBehaviour
{
	public static int fileID = 0;
	private int count = 0;
    // Start is called before the first frame update
    void OnEnable()
    {
        
    }
	
	public void ConfirmDelete()
	{
		GameDataHolder.DeleteFile(fileID);
		ReturnToMain();
	}
	
	public void ReturnToMain()
	{
		count++;
		if(count == 4)
			PlayerMovement.ExtraSpoils = true;
		FindObjectOfType<TitleMenu>().SetPanelID(0);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
