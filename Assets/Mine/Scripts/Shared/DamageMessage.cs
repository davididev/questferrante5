﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageMessage 
{
    public int Amount;
	public ELEMENT ElementID;
    public float dir;
	public enum ELEMENT { Weapon, Fire, Water, Earth, Wind, Lightning, Dark, Light, Nil}
	
    /// <summary>
    /// Create a new Damage message
    /// </summary>
    /// <param name="a">Amount</param>
    /// <param name="e">Element</param>
    /// <param name="d">Direction</param>
	public DamageMessage(int a, ELEMENT e, float d = 0f)
	{
		Amount = a;
		ElementID = e;
        dir = d;
	}
}
