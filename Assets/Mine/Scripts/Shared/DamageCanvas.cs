﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCanvas : MonoBehaviour
{
    public TMPro.TextMeshProUGUI textObj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ShowText(string s)
    {
        textObj.text = s;
        textObj.gameObject.transform.localScale = Vector3.one * 0.5f;
        float t = 0.5f;  //Time of show text
        Vector3 newPos = transform.position + (Vector3.up * 1f);
        iTween.MoveTo(gameObject, newPos, t);
        iTween.ScaleTo(textObj.gameObject, Vector3.one, t);
        Invoke("Vanish", 1f);
    }

    void Vanish()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
