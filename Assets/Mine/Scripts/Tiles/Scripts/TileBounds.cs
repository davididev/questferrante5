﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileBounds : MonoBehaviour
{
	public TilemapCollider2D col;
	private Bounds bounds;
	
	public static Vector3 topLeft, bottomRight;
	
    // Start is called before the first frame update
    void Start()
    {
        bounds = col.bounds;  //There, piece of the cake.  Now we know how big the map is :D
    }

    // Update is called once per frame
    void Update()
    {
        if(bounds == null)
			bounds = col.bounds;  //There, piece of the cake.  Now we know how big the map is :D
		
		topLeft = new Vector3((bounds.center.x - bounds.extents.x), (bounds.center.y + bounds.extents.y), 0f);
		bottomRight =  new Vector3((bounds.center.x + bounds.extents.x), (bounds.center.y - bounds.extents.y), 0f);
    }
}
