﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBlock : MonoBehaviour
{
	public GameObject splashPrefab;
	private const float MAX_VERTICAL_SPEED = 0.1f;
	private const float MAX_VERTICAL_MOVEMENT = 0.5f;
	
	public int damagePerSecond = 0;
	private float damageTimer = 0f;
	private float startingY;
	private bool goingUp = false;
	
	public bool dontMove = false;
	
    // Start is called before the first frame update
    void Start()
    {
        startingY = transform.position.y;
		
		if(splashPrefab != null)
		{
			GameObjectPool.InitPoolItem("Splash", splashPrefab, 10);
		}
    }

    // Update is called once per frame
    void Update()
    {
		if(dontMove == false)
		{
			Vector3 targetPos = transform.position;
			if(goingUp == true)
				targetPos.y = startingY + MAX_VERTICAL_MOVEMENT;
			else
				targetPos.y = startingY - MAX_VERTICAL_MOVEMENT;
		
			transform.position = Vector3.MoveTowards(transform.position, targetPos, MAX_VERTICAL_SPEED * Time.deltaTime);
			if(Vector3.Distance(transform.position, targetPos) < 0.01f)
				goingUp = !goingUp;
		}
		
		if(damagePerSecond > 0)
		{
			if(ph != null)
			{
				damageTimer += Time.deltaTime;
				if(damageTimer >= 1f)
				{
					damageTimer -= 1f;
					ph.Damage(new DamageMessage(damagePerSecond, DamageMessage.ELEMENT.Fire, 0f));
				}
			}
		}
    }
	
	void Splash(Vector3 playerPos)
	{
		Vector3 newPos = playerPos;
		BoxCollider2D b = GetComponent<BoxCollider2D>();
		float h = b.size.y / 2f;
		newPos.y = transform.position.y + b.offset.y + h + 0.1f;
		
		GameObject splorsh = GameObjectPool.GetInstance("Splash", newPos, Quaternion.identity);
	}
	
	private PlayerHealth ph;
	
	void OnTriggerExit2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			col.GetComponent<PlayerMovement>().moveSpeedScale = 1f;
			Splash(col.transform.position);
			ph = null;
		}
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			col.GetComponent<PlayerMovement>().moveSpeedScale = 0.5f;
			ph = col.GetComponent<PlayerHealth>();
			Splash(col.transform.position);
			damageTimer = 1f;
		}
	}
}
