﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public TrailRenderer trailRend;
    public int playerDamage = 10;
    public DamageMessage.ELEMENT element = DamageMessage.ELEMENT.Fire;

    

    public float VELOCITY_PER_SECOND = 5f;

	public float gravityScale = 0f;
	
	private float gravity = 0f;
    private Rigidbody2D rigid;

    // Start is called before the first frame update
    void OnEnable()
    {
        gravity = 0f;
    }
	
	public void SetGravity(float g)
	{
		gravity = g;
	}
    
    

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();

        Vector2 vel = Vector2.right * VELOCITY_PER_SECOND;
        vel.x *= transform.localScale.normalized.x;
		gravity -= gravityScale * 9.8f * Time.fixedDeltaTime;
		vel.y = gravity;

        rigid.velocity = vel;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (trailRend != null)
            trailRend.Clear();

        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.SendMessage("Damage", new DamageMessage(playerDamage, DamageMessage.ELEMENT.Fire, transform.localScale.x));
        }

        gameObject.SetActive(false);
    }
}
