﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaspEnemy : Enemy
{
	public float speedCharge = 8f, speedSlow = 1f;
	private bool hitPlayer = false, hitWall = false;
	protected override IEnumerator MainRoutine()
    {
        yield return new WaitForFixedUpdate();
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		while(gameObject)
		{
			targetPos = Vector2.zero;
			float dist = Vector3.Distance(player.transform.position, transform.position);
			if(dist < 8f)
			{
				//print("Close.");
				maxVelocity = speedCharge;
				targetPos = player.transform.position;
				float dist2 = Vector2.Distance(rigid.position, targetPos);
				hitPlayer = false;
				hitWall = false;
				while(dist2 > 1f)
				{
					 dist2 = Vector2.Distance(rigid.position, targetPos);
					 if(hitPlayer == true)
						 dist2 = 0f;
					 if(hitWall == true)
					 {
						 dist2 = 0f;
						 if(ignoreGravity == true)  //Only damage you for crashing into walls if you can fly
							gameObject.SendMessage("Damage", new DamageMessage(3, DamageMessage.ELEMENT.Weapon, 0f), SendMessageOptions.DontRequireReceiver);
					 }
					 yield return new WaitForFixedUpdate();
				}
				//print("Dash over.");
				targetPos = rigid.position + (Vector2.up * 0.5f);
				yield return new WaitForSeconds(2f);
				
			}
			//print("Far away.");

			maxVelocity = speedSlow;
			targetPos = new Vector2(player.transform.position.x, player.transform.position.y);
			yield return new WaitForFixedUpdate();
		}
	}
    protected override void PlayerCollision(Collision2D collision)
	{
		hitPlayer = true;
	}
	
	protected override void DefaultCollision(Collision2D collision)
	{
		hitWall = true;
	}
	
	public void Damage(DamageMessage msg)
	{
		hitWall = true;
	}
}
