﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillerRobotEnemy : Enemy
{
    public EnemyHealth hp;
    public GameObject bulletPrefab;
    public AudioClip bossMusic, originalMusic;
    public TextAsset onDeathDialogue;
    public SpriteRenderer forcefieldRenderer;
    private float defensePerc = 0f;

    private const float PERC_PER_SECOND = 0.10f;  //Takes ten seconds to fully clear forcefield
    private const float PERC_PER_HIT = 0.25f;  //Increase forcefield by 25% every time you hit the enemy
    private const float BULLET_PER_SECOND_TIME = 0.2f, STREAM_TIMER = 5f;
    private const int STREAM_COUNT = 15;
    private int bulletCount = 0;
    private GameObject player;
    public AudioClip pistolFireSoundFX;

    public const float MAX_DEFENSE = 70f;
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        GameObjectPool.InitPoolItem("Boss Bullet", bulletPrefab, 30);
        while (DialogueHandler.IS_RUNNING)
        {
            yield return new WaitForEndOfFrame();
        }

        FindObjectOfType<PlayMusic>().PlaySong(bossMusic);

        player = null;
        while (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForEndOfFrame();
        }

        while(gameObject.activeSelf)
        {
            float x = 1f;
            float y = 1f;
            if (player.transform.position.x < gameObject.transform.position.x)
                x = -1f;
            if (player.transform.position.y < gameObject.transform.position.y)
                y = -1f;
            Vector2 v = new Vector2(x * 50f, y * 20f);
            rigid.AddForce(v, ForceMode2D.Impulse);
            yield return new WaitForSeconds(1f);

            
        }

        FindObjectOfType<PlayMusic>().PlaySong(bossMusic);
    }

    public void Damage(DamageMessage msg)
    {
        if (msg.ElementID == DamageMessage.ELEMENT.Earth)
            defensePerc += PERC_PER_HIT * 0.5f;
        else
            defensePerc += PERC_PER_HIT;
        if (defensePerc > 1f)
            defensePerc = 1f;

    }

    float bulletTimer = BULLET_PER_SECOND_TIME;
    protected override void OnFixedUpdate()
    {
        defensePerc -= Time.fixedDeltaTime * PERC_PER_SECOND;
        if (defensePerc < 0f)
            defensePerc = 0f;

        Color c = forcefieldRenderer.color;
        c.a = defensePerc;
        forcefieldRenderer.color = c;
        float defense = MAX_DEFENSE * defensePerc;

        hp.defense = (int)defense;

        if(player != null)
        {
            
            bulletTimer -= Time.fixedDeltaTime;
            if (bulletTimer < 0f)
            {
                bulletCount++;
                AudioSource.PlayClipAtPoint(pistolFireSoundFX, Camera.main.transform.position);
                float x = 1f;
                float y = 1f;
                if (player.transform.position.x < gameObject.transform.position.x)
                    x = -1f;
                if (player.transform.position.y < gameObject.transform.position.y)
                    y = -1f;
                Vector3 bulletPos = transform.position + (Vector3.right * x * 1.5f);
                GameObject bullet = GameObjectPool.GetInstance("Boss Bullet", bulletPos, Quaternion.identity);
                bullet.transform.localScale = new Vector3(x * 2f, 2f, 2f);
                if(bulletCount > STREAM_COUNT)  //Bullet stream over
                {
                    bulletCount = 0;
                    bulletTimer = STREAM_TIMER;
                }
                else
                    bulletTimer = BULLET_PER_SECOND_TIME;
            }
        }
        
    }

    protected override void EnemyDeath()
    {
        FindObjectOfType<PlayMusic>().PlaySong(originalMusic);
        FindObjectOfType<DialogueHandler>().StartEvent(onDeathDialogue);
    }
}
