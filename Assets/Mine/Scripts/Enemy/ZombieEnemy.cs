﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieEnemy : Enemy
{
    public GameObject explosionPrefab;
    protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("Zombie Explosion", explosionPrefab, 10);
        
    }

    protected override IEnumerator MainRoutine()
    {
        yield return null;


        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Animator anim = GetComponent<Animator>();
        while (gameObject)
        {
            targetPos = player.transform.position;
            yield return new WaitForSeconds(0.5f);
            if (Vector3.Distance(transform.position, player.transform.position) < 3.5f)
            {
                GameObject g = GameObjectPool.GetInstance("Zombie Explosion", transform.position, Quaternion.identity);
                if (g != null)
                {
                    targetPos = Vector2.zero;
                    yield return new WaitForSeconds(2f);
                }

            }
        }
    }

}
