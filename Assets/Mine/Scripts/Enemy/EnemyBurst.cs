﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBurst : MonoBehaviour
{
    public int DamageAmt = 75;
    public DamageMessage.ELEMENT Ele;

    void OnTriggerEnter2D(Collider2D c)
    {
        if(c.tag == "Player")
        {
            c.gameObject.SendMessage("Damage", new DamageMessage(DamageAmt, Ele, 0f));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
