﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    private Rigidbody2D rigid;
    private SpriteRenderer rend;
    [HideInInspector] public int[] multipliers = { 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100};
    public int maxHealth = 10, defense = 0;
    private int minHealth;
    // Start is called before the first frame update
    void OnEnable()
    {
        minHealth = maxHealth;

    }

    /// <summary>
    /// Should be called by GameObject.SendMessage.
    /// </summary>
    /// <param name="msg"></param>
	public void Damage(DamageMessage msg)
    {
        Debug.Log("Damaging " + gameObject.name + ".  Initial info: " + msg.Amount + "," + msg.ElementID);
        int amt = msg.Amount;
        amt = amt * multipliers[(int)msg.ElementID] / 100;
        amt -= defense;

        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();

        if(amt > 0)
        {
            Debug.Log("Damaging " + gameObject.name + " for " + amt + " HP");
            rigid.AddForce(Vector2.right * msg.dir * 25f, ForceMode2D.Impulse);
            StartCoroutine(Flash());

            GameObject txt = GameObjectPool.GetInstance("DMG", transform.position, Quaternion.identity);
            txt.SendMessage("ShowText", ("<color=red>-" + amt + " HP"));

            minHealth -= amt;
        }

        if (minHealth <= 0)
            gameObject.SendMessage("OnDeath");
    }

    IEnumerator Flash()
    {
        if (rend == null)
            rend = GetComponent<SpriteRenderer>();

        rend.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        rend.color = Color.white;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
