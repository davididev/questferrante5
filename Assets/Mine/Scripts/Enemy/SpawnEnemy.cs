﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
	public float minTimer = 50f, maxTimer = 80f;
	
	private float timerTarget = 0f;
	private float currentTimer = 0f;
	
	public int enemyID = 0;  //Use SceneVars' enemy pool
	
    // Start is called before the first frame update
    void Start()
    {
        timerTarget = Random.Range(minTimer, maxTimer);
		currentTimer = timerTarget * 0.9f;
    }

    // Update is called once per frame
    void Update()
    {
        currentTimer += Time.deltaTime;
		if(currentTimer >= timerTarget)
		{
			Vector3 viewport = Camera.main.WorldToViewportPoint(transform.position);
			//Is on screen?
			if(viewport.x >= 0f && viewport.x <= 1f && viewport.y >= 0f && viewport.y <= 1f)
			{
				currentTimer = 0f;
			
				GameObject g = GameObjectPool.GetInstance("Enemy" + enemyID, transform.position, Quaternion.identity);
				if(g == null)
				{
					currentTimer = timerTarget * 0.75f;
				}
				else
				{
					timerTarget = Random.Range(minTimer, maxTimer);
				}
			}
			else
			{
				currentTimer = timerTarget - 1f;
			}
		}
    }
}
