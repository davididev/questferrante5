﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyfishEnemy : Enemy
{
    public GameObject fireballPrefab;
	protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("JellyfishLightning", fireballPrefab, 50);
    }
	
	protected override IEnumerator MainRoutine()
    {
        yield return new WaitForFixedUpdate();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
		Animator anim = GetComponent<Animator>();
		bool foundPlayer = false;
		while(gameObject)
		{
			Vector2 startingPos = rigid.position;
			float floatUpTimer = 1f;
			float floatDownTimer = 0f;
			while(foundPlayer == false)
			{
				if(floatUpTimer > 0f)
				{
					targetPos = startingPos + Vector2.up;
					floatUpTimer -= Time.deltaTime;
					floatDownTimer = 1f;
				}
				if(floatDownTimer > 0f)
				{
					targetPos = startingPos + Vector2.down;
					floatDownTimer -= Time.deltaTime;
					floatUpTimer = 1f;
				}
				
				if(Vector3.Distance(transform.position, player.transform.position) < 6f)
					foundPlayer = true;
				
				yield return new WaitForFixedUpdate();
			}
			
			float timer = 2.5f;
			while(timer > 0f)
			{
				targetPos = new Vector2(player.transform.position.x, player.transform.position.y);
				timer -= Time.fixedDeltaTime;
				yield return new WaitForFixedUpdate();
			}
			
			ClearTargetPos();
			yield return new WaitForSeconds(1.5f);
			
			Vector3 bulletPos = transform.position + (Vector3.up * 0.5f);
			ShootBullet(bulletPos, anim);
			bulletPos = transform.position + (Vector3.down * 0.5f);
			ShootBullet(bulletPos, anim);

		}
	}
	
	void ShootBullet(Vector3 pos, Animator anim)
	{
		GameObject bullet = GameObjectPool.GetInstance("JellyfishLightning", pos + Vector3.left, Quaternion.identity);
        if (bullet != null)
        {
            bullet.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
		
		GameObject bullet2 = GameObjectPool.GetInstance("JellyfishLightning", pos + Vector3.right, Quaternion.identity);
        if (bullet2 != null)
        {
            bullet2.transform.localScale = new Vector3(1f, 1f, 1f);
        }
	}
}
