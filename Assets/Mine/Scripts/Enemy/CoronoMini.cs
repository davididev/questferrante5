﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoronoMini : Enemy
{
    public static int Count = 0;
    private void OnEnable()
    {
        Count++;
    }

    private void OnDisable()
    {
        Count--;
    }

    protected override IEnumerator MainRoutine()
    {
        yield return null;

        GameObject player = null;
        while (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForEndOfFrame();
        }

        while (gameObject)
        {
            targetPos = player.transform.position;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
