﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicalNoteEnemy : Enemy
{
    float horizontalDirection = -1f;
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForFixedUpdate();
        while (gameObject)
        {
            targetPos = rigid.position + (Vector2.right * horizontalDirection * 5f);
            Jump();
            yield return new WaitForFixedUpdate();
        }
    }

    protected override void DefaultCollision(Collision2D collision)
    {
        bool failure = true;
        for(int i = 0; i < collision.contactCount; i++)
        {
            ContactPoint2D pt = collision.GetContact(i);
            if (Mathf.Approximately(pt.normal.y, 0f))
            {
                failure = false;
                break;
            }
                
        }
        if (failure == false)
            horizontalDirection *= -1f;
    }

    protected override void PlayerCollision(Collision2D collision)
    {
        bool failure = true;
        for (int i = 0; i < collision.contactCount; i++)
        {
            ContactPoint2D pt = collision.GetContact(i);
            if (Mathf.Approximately(pt.normal.y, 0f))
            {
                failure = false;
                break;
            }

        }
        if (failure == false)
            horizontalDirection *= -1f;
    }
}
