﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonBoss : Enemy
{
	public GameObject fireballPrefab;
	private Vector2 startingPos;
	public string varName = "%dragon1_Kavu";
	public float TOP_Y = 9f, BOT_Y = 2f;
	
	protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("DragonBossFireball", fireballPrefab, 50);
    }
	
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForEndOfFrame();
		startingPos = rigid.position;
		bool goingUp = true;
		int seed = 0;
		float bulletTimer = 0f;
		while(gameObject)
		{
			if(goingUp == true)
			{
				this.targetPos = startingPos;
				this.targetPos.y = TOP_Y;
				
				if(rigid.position.y >= TOP_Y - 0.5f)
					goingUp = false;
			}
			if(goingUp == false)
			{
				this.targetPos = startingPos;
				this.targetPos.y = BOT_Y;
				
				if(rigid.position.y <= BOT_Y + 0.5f)
					goingUp = true;
			}
			
			if(bulletTimer <= 0f)
			{
				Vector3 bulletPos = transform.position + (Vector3.left * 1.5f);
				GameObject bullet = GameObjectPool.GetInstance("DragonBossFireball", bulletPos, Quaternion.identity);
				bullet.transform.localScale = new Vector3(-2f, 2f, 2f);
				Random.InitState(seed);
				bulletTimer = Random.Range(1f, 4f);
				seed++;
			}
			if(bulletTimer > 0f)
				bulletTimer -= Time.deltaTime;
			
			yield return new WaitForEndOfFrame();
		}
	}
	
	protected override void EnemyDeath()
    {
		FindObjectOfType<SceneVars>().PlayMusic();  //Play default music
		
		VarsDB.instance.SetVarValue(varName, 1);
		PlayerMovement.LockScreen = false;
    }
}
