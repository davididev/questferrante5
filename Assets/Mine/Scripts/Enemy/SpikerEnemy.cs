﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikerEnemy : Enemy
{
	public LayerMask lm;
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForEndOfFrame();
		bool goingLeft = true;
		
		while(gameObject.activeSelf)
		{
			//Vector2 pv = new Vector2(transform.position.x, transform.position.y);
			Vector2 pv = rigid.position;
			if(goingLeft == true)
			{
				Vector2 v = pv + (Vector2.left * 3f);
				targetPos = v;
				
				if(Physics2D.Raycast(pv, Vector2.left, 1f, lm))
					goingLeft = false;
			}
			else
			{
				Vector2 v = pv + (Vector2.right * 3f);
				targetPos = v;
				
				if(Physics2D.Raycast(pv, Vector2.right, 1f, lm))
					goingLeft = true;
			}
			yield return new WaitForEndOfFrame();
		}
	}
}
