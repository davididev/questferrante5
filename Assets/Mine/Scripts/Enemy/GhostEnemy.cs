﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostEnemy : Enemy
{
    public GameObject fireballPrefab;
	protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("GhostFireball", fireballPrefab, 50);
    }
	
	protected override IEnumerator MainRoutine()
    {
        yield return new WaitForFixedUpdate();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
		Animator anim = GetComponent<Animator>();
		bool foundPlayer = false;
		while(gameObject)
		{
			Vector2 startingPos = rigid.position;
			float floatUpTimer = 1f;
			float floatDownTimer = 0f;
			while(foundPlayer == false)
			{
				if(floatUpTimer > 0f)
				{
					targetPos = startingPos + Vector2.up;
					floatUpTimer -= Time.deltaTime;
					floatDownTimer = 1f;
				}
				if(floatDownTimer > 0f)
				{
					targetPos = startingPos + Vector2.down;
					floatDownTimer -= Time.deltaTime;
					floatUpTimer = 1f;
				}
				
				if(Vector3.Distance(transform.position, player.transform.position) < 6f)
					foundPlayer = true;
				
				yield return new WaitForFixedUpdate();
			}
			targetPos = new Vector2(player.transform.position.x, player.transform.position.y);
			yield return new WaitForSeconds(5f);
			Vector3 bulletPos = transform.position;
			ShootBullet(bulletPos, anim);
			yield return new WaitForSeconds(0.2f);
			bulletPos.y += 1f;
			ShootBullet(bulletPos, anim);
			yield return new WaitForSeconds(0.2f);
			bulletPos.y -= 1f;
			ShootBullet(bulletPos, anim);
		}
	}
	
	void ShootBullet(Vector3 pos, Animator anim)
	{
		GameObject bullet = GameObjectPool.GetInstance("GhostFireball", pos, Quaternion.identity);
            if (bullet != null)
            {
				anim.SetTrigger("Attack");
                if (rend.flipX)
                    bullet.transform.localScale = new Vector3(1f, 1f, 1f);
                else
                    bullet.transform.localScale = new Vector3(-1f, 1f, 1f);
            }
	}
	
	
	
}
