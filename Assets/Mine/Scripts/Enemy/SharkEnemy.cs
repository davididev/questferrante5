﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkEnemy : Enemy
{
    public EnemyHealth hp;
	public GameObject fireballPrefab;
	public GameObject forcefieldObj;
	private int hitCounter = 0;
	
	private const int EARTH_COUNTER = 5;
	
	
	protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("SharkFireball", fireballPrefab, 50);
		SetForcefield();
    }
	
	private void SetForcefield()
	{
		bool isForcefield = false;
		
		if(hitCounter % EARTH_COUNTER == 4)
			isForcefield = true;
		
		if(isForcefield)
		{
			forcefieldObj.SetActive(true);
			for(int i = 0; i < hp.multipliers.Length; i++)
			{
				if(i == 3)  //Earth
					hp.multipliers[i] = 200;
				else
					hp.multipliers[i] = 0;
			}
			
			
		}
		else
		{
			forcefieldObj.SetActive(false);
			for(int i = 0; i < hp.multipliers.Length; i++)
			{
				hp.multipliers[i] = 0;
				if(i == 0)  //Weapon
					hp.multipliers[i] = 50;
				if(i == 1)  //Fire
					hp.multipliers[i] = 100;
			}
		}
		
	}
	
	protected override IEnumerator MainRoutine()
    {
        yield return new WaitForFixedUpdate();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
		
		while(DialogueHandler.IS_RUNNING)
		{
			yield return new WaitForFixedUpdate();
		}
		
		yield return new WaitForFixedUpdate();
		while(DialogueHandler.IS_RUNNING)
		{
			yield return new WaitForFixedUpdate();
		}
		
		while(gameObject)
		{
			for(int i = 0; i < 12; i++)
			{
				yield return new WaitForSeconds(0.5f);
				Random.InitState(i);
				float r = Random.Range(-5f, 5f);
				if(r > -1f && r < 1f)
					r = -1f;
				targetPos = new Vector2(player.transform.position.x + r, player.transform.position.y + 1.5f	);
				if(i < 6)
					ThrowFireball(player.transform.position);
			}
			
		}
	}
	
	void ThrowFireball(Vector3 playerPos)
        {
            Vector3 bulletPos = transform.position + (Vector3.up * 1.25f);
            GameObject bullet = GameObjectPool.GetInstance("SharkFireball", bulletPos, Quaternion.identity);
            if (bullet != null)
            {
                if (playerPos.x > transform.position.x)
                    bullet.transform.localScale = new Vector3(1f, 1f, 1f);
                else
                    bullet.transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }
	
	public void Damage(DamageMessage msg)
	{
		bool isForcefield = forcefieldObj.activeSelf;
		bool counter = false;
		if(isForcefield == true && msg.ElementID == DamageMessage.ELEMENT.Earth)
			counter = true;
		if(isForcefield == false && msg.ElementID != DamageMessage.ELEMENT.Earth)
			counter = true;
		
		
		if(counter)
		{
			hitCounter++;
			SetForcefield();
		}
		
	}
	
	protected override void EnemyDeath()
    {
		FindObjectOfType<SceneVars>().PlayMusic();  //Play default music
		
		VarsDB.instance.SetVarValue("%story", 12);
		PlayerMovement.LockScreen = false;
    }
}
