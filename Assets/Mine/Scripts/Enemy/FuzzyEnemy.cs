﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuzzyEnemy : Enemy
{
	public string jumpTriggerName = "";
    public float jumpHori = 5f;
	public Animator anim;
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForEndOfFrame();
        GameObject player = null;
        while(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForEndOfFrame();
        }
        while(gameObject.activeSelf)
        {
			if(anim == null && jumpTriggerName != "")
				anim = GetComponent<Animator>();
            if(isGrounded)
            {
				if(jumpTriggerName != "")
					anim.SetTrigger(jumpTriggerName);
                float xDist = Mathf.Abs(transform.position.x - player.transform.position.x);
                float dir = -1f;
                if (transform.position.x < player.transform.position.x)
                    dir = 1f;
                if(xDist < 5f)
                {
                    Vector2 rel = new Vector2(dir * jumpHori, jumpHeight);
                    rigid.AddForce(rel, ForceMode2D.Impulse);
                    yield return new WaitForSeconds(0.5f);
                }
                if (xDist >= 5f && xDist < 12f)
                {
                    Vector2 rel = new Vector2(dir * jumpHori * 3f, jumpHeight);
                    rigid.AddForce(rel, ForceMode2D.Impulse);
                    yield return new WaitForSeconds(0.5f);
                }
                if(xDist >= 12f)
                {
                    Vector2 rel = new Vector2(0f, jumpHeight);
                    rigid.AddForce(rel, ForceMode2D.Impulse);
                    yield return new WaitForSeconds(0.5f);
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
