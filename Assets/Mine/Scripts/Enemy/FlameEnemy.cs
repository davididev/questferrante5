﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameEnemy : Enemy
{
	public static int FlameCount = 0;
	public TextAsset onStoryComplete;
    protected override void OnEnemyStart()
	{
		Debug.Log("Current story: " + VarsDB.instance.GetVarValue("%story"));
		if(VarsDB.instance.GetVarValue("%story") >= 2)
		{
			gameObject.SetActive(false);
			return;
		}
		FlameCount++;
	}
	protected override void DisableEnemy()
	{
		FlameCount--;
	}
	
	protected override void EnemyDeath()
	{
		if(FlameCount <= 1)
		{
			FindObjectOfType<DialogueHandler>().StartEvent(onStoryComplete);
			
		}
	}
}
