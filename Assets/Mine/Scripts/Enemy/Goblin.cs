﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Enemy
{
    public GameObject fireballPrefab;
    public float xDist = 5f;

    protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("GoblinFireball", fireballPrefab, 50);
    }
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForFixedUpdate();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Animator anim = GetComponent<Animator>();
        float dist = 20f;
        while (gameObject)
        {

            Vector2 startingPos = rigid.position;
            bool wanderLeft = true;
            
            while (dist > (xDist + 1f))
            {

                dist = Mathf.Abs(player.transform.position.x - transform.position.x);

                if(wanderLeft)
                    this.targetPos = startingPos + (Vector2.left * xDist);
                else
                    this.targetPos = startingPos + (Vector2.right * xDist);

                if (Mathf.Abs(rigid.position.x - targetPos.x) <= 1f)
                {
                    wanderLeft = !wanderLeft;
                    ThrowFireball();
                    Vector2 prevTarget = targetPos;
                    targetPos = Vector2.zero;
                    yield return new WaitForSeconds(0.4f);
                    targetPos = prevTarget;
                }
                    

                yield return new WaitForFixedUpdate();

            }

            while(dist < (xDist + 10f))
            {
                for(int i = 0; i < 10; i++)
                {
                    targetPos = player.transform.position;
                    yield return new WaitForSeconds(0.25f);
                }
                ThrowFireball();
                targetPos = Vector2.zero;
                yield return new WaitForSeconds(0.4f);
                targetPos = player.transform.position;
                dist = Mathf.Abs(player.transform.position.x - transform.position.x);
            }
            
        }

        void ThrowFireball()
        {
            anim.SetTrigger("Attack");
            Vector3 bulletPos = transform.position + (Vector3.up * 1.25f);
            GameObject bullet = GameObjectPool.GetInstance("GoblinFireball", bulletPos, Quaternion.identity);
            if (bullet != null)
            {
                if (rend.flipX)
                    bullet.transform.localScale = new Vector3(1f, 1f, 1f);
                else
                    bullet.transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }
		
		
    }
}
