﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncySpikerEnemy : Enemy
{
    private int currentDirection = 0;
    public float CornerVelocity = 5f;

    public LayerMask lm;


    protected override void OnEnemyStart()
    {
        
    }

    protected override IEnumerator MainRoutine()
    {
        
        yield return new WaitForEndOfFrame();

        while(gameObject)
        {
            Debug.Log("Current Dir: " + currentDirection);


            if (currentDirection == 0)
                rigid.velocity = new Vector2(CornerVelocity, CornerVelocity);
            if (currentDirection == 1)
                rigid.velocity = new Vector2(CornerVelocity, -CornerVelocity);
            if (currentDirection == 2)
                rigid.velocity = new Vector2(-CornerVelocity, -CornerVelocity);
            if (currentDirection == 3)
                rigid.velocity = new Vector2(-CornerVelocity, CornerVelocity);
                

            if (currentDirection > 3)
                currentDirection = 0;

            if (transform.position.x > TileBounds.bottomRight.x)
                currentDirection = 2;
            if (transform.position.x < TileBounds.topLeft.x)
                currentDirection = 0;

            if (directionTimer > 0f)
                directionTimer -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        
    }

    float directionTimer = 0f;

    void OnCollisionStay2D(Collision2D collision)
    {
        if(directionTimer <= 0f)
        {
            currentDirection++;
            directionTimer = 0.05f;
        }
        
    }


}
