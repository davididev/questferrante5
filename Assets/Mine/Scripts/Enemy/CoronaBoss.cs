﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoronaBoss : Enemy
{
    public AudioClip teleportSound;
    public AudioClip bossMusic, originalMusic;
    public GameObject miniPrefab;
    public TextAsset onDeathDialogue;
    public EnemyHealth enemHealth;
    private const int touchStart = 90;
    protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("Mini Corona", miniPrefab, 30);
    }

    protected override void EnemyDeath()
    {
        FindObjectOfType<PlayMusic>().PlaySong(originalMusic);
        FindObjectOfType<DialogueHandler>().StartEvent(onDeathDialogue);
    }

    protected override void OnFixedUpdate()
    {
        enemHealth.defense = CoronoMini.Count * 5;
        touchDamage = touchStart * CoronoMini.Count * 2 / 5;
    }

    public void Damage(DamageMessage msg)
    {
        float leftDist = 0f;
        float rightDist = 0f;
        RaycastHit2D hitLeft = Physics2D.Raycast(rigid.position, Vector2.left, 300f, LayerMask.GetMask("Default"));
        leftDist = rigid.position.x - hitLeft.point.x;
        RaycastHit2D hitRight = Physics2D.Raycast(rigid.position, Vector2.right, 300f, LayerMask.GetMask("Default"));
        rightDist = hitRight.point.x - rigid.position.x;

        if(leftDist > rightDist)
        {
            //Teleport to the left
            Vector2 pt = hitLeft.point + Vector2.right;
            rigid.position = pt;
        }
        if (rightDist > leftDist)
        {
            //Teleport to the left
            Vector2 pt = hitRight.point + Vector2.left;
            rigid.position = pt;
        }
        AudioSource.PlayClipAtPoint(teleportSound, Camera.main.transform.position);
    }




    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        while(DialogueHandler.IS_RUNNING)
        {
            yield return new WaitForEndOfFrame();
        }
        
        GameObject player = null;
        while (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForEndOfFrame();
        }

        FindObjectOfType<PlayMusic>().PlaySong(bossMusic);

        StartCoroutine(SpawnEnemies());
        while (gameObject)
        {
            targetPos = player.transform.position;
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator SpawnEnemies()
    {
        float spawnTime = 5f;
        while(gameObject)
        {
            Vector3 rel = Vector3.left + (Vector3.up * Random.Range(-2f, 2f));
            GameObject g = GameObjectPool.GetInstance("Mini Corona", transform.position + rel, Quaternion.identity);
            if(g != null)
            {
                spawnTime = spawnTime * 0.75f;
                if (spawnTime < 1f)
                    spawnTime = 1f;
                yield return new WaitForSeconds(spawnTime);
            }

            yield return new WaitForSeconds(0.2f);
        }
    }
}
