﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailEnemy : Enemy
{
	public GameObject bulletPrefab;
	private Animator anim;
	
	protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("SnailBullet", bulletPrefab, 50);
    }
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForEndOfFrame();
        GameObject player = null;
        while(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForEndOfFrame();
        }
		
		while(gameObject.activeSelf)
		{
			float xDist = Mathf.Abs(transform.position.x - player.transform.position.x);
			targetPos = player.transform.position;
			if(Mathf.Abs(xDist) < 10f)
			{
				//You're close!
				float direction = 1f;
				Vector2 pv = new Vector2(player.transform.position.x, player.transform.position.y);
				if(player.transform.position.x > transform.position.x)
					targetPos = pv + (Vector2.right * 5f);
				if(player.transform.position.x < transform.position.x)
				{
					direction = -1f;
					targetPos = pv + (Vector2.left * 5f);
				}
				
				if(anim == null)
					anim = GetComponent<Animator>();
				for(int i = 0; i < 4; i++)
				{
					anim.SetTrigger("Attack");
					yield return new WaitForSeconds(0.2f);
					Vector3 bulletPos = transform.position + Vector3.up + (Vector3.right * -1f);
					
					GameObject bullet = GameObjectPool.GetInstance("SnailBullet", bulletPos, Quaternion.identity);
					if (bullet != null)
					{
						bullet.SendMessage("SetGravity", 2f);
						bullet.transform.localScale = new Vector3(-1f, 1f, 1f);	
					}
					bulletPos = transform.position + Vector3.up + (Vector3.right * 1f);
					GameObject bullet2 = GameObjectPool.GetInstance("SnailBullet", bulletPos, Quaternion.identity);
					if (bullet2 != null)
					{
						bullet2.SendMessage("SetGravity", 2f);
						bullet2.transform.localScale = new Vector3(1f, 1f, 1f);	
					}
					
				}
				ClearTargetPos();
				yield return new WaitForSeconds(1f);
			}
			
			ClearTargetPos();
			yield return new WaitForSeconds(1f);
		}
	}
}
