﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionEnemy : Enemy
{
	public GameObject bulletPrefab;
	
	
	protected override void OnEnemyStart()
    {
        GameObjectPool.InitPoolItem("ScorpionBullet", bulletPrefab, 50);
    }
    protected override IEnumerator MainRoutine()
    {
        yield return new WaitForEndOfFrame();
        GameObject player = null;
        while(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            yield return new WaitForEndOfFrame();
        }
		
		while(gameObject.activeSelf)
		{
			float xDist = Mathf.Abs(transform.position.x - player.transform.position.x);
			
			if(Mathf.Abs(xDist) < 10f)
			{
				//You're close!
				float direction = 1f;
				Vector2 pv = new Vector2(player.transform.position.x, player.transform.position.y);
				if(player.transform.position.x > transform.position.x)
					targetPos = pv + (Vector2.right * 5f);
				if(player.transform.position.x < transform.position.x)
				{
					direction = -1f;
					targetPos = pv + (Vector2.left * 5f);
				}
				
				for(int i = 0; i < 4; i++)
				{
					yield return new WaitForSeconds(0.2f);
					Vector3 bulletPos = transform.position + Vector3.up + (Vector3.right * direction);
					
					GameObject bullet = GameObjectPool.GetInstance("ScorpionBullet", bulletPos, Quaternion.identity);
					if (bullet != null)
					{
						bullet.SendMessage("SetGravity", 2f);
						bullet.transform.localScale = new Vector3(direction, 1f, 1f);	
					}
					
				}
				ClearTargetPos();
				yield return new WaitForSeconds(1f);
			}
			
			ClearTargetPos();
			yield return new WaitForSeconds(1f);
		}
	}
}
