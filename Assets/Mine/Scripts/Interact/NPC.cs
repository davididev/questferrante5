﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public TextAsset dialogue, beckyDialogue, johnnyDialogue;
    public float timeBeforeNextDialogue = 15f;
    private float timer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(DialogueHandler.IS_RUNNING == false)
        {
            if (timer > 0f)
                timer -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
		float interactDifference = Time.time - BoilerRoot.START_TIME;
		if(interactDifference < 2f)  //The file was just loaded- don't process interactions		
			return;
		if(DialogueHandler.IS_RUNNING == true)
			return;
        if(timer <= 0f)
        {
            if (collision.tag == "Player")
            {
				timer = timeBeforeNextDialogue;
				bool isBecky = true;
				if(GameDataHolder.instance.currentHero == 1 || GameDataHolder.instance.currentHero == 3)
					isBecky = false;
				
				if(isBecky && beckyDialogue != null)
				{
					FindObjectOfType<DialogueHandler>().StartEvent(beckyDialogue);
					return;
				}
				
				if(isBecky == false && johnnyDialogue != null)
				{
					FindObjectOfType<DialogueHandler>().StartEvent(johnnyDialogue);
					return;
				}
                FindObjectOfType<DialogueHandler>().StartEvent(dialogue);
                
            }
        }
        
    }
}
