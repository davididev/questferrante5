﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopKeep : MonoBehaviour
{
	public ItemToBuy[] itemsToBuy;
	private float itemTimer = 0f;
	private const float ITEM_DELAY = 15f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(itemTimer > 0f)
			itemTimer -= Time.deltaTime;
    }
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(itemTimer > 0f)
			return;
		if(col.tag == "Player")
		{
			itemTimer = ITEM_DELAY;
			ShopMenu.ItemsInShop = itemsToBuy;
			Camera.main.GetComponent<CameraUI>().ShowShop();
		}
	}
}
