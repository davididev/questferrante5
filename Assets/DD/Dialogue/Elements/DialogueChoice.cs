﻿using System.Collections;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.EventSystems;

public class DialogueChoice : DialogueItem
{
	public static int SELECTED_CHOICE = -1;
    public string promptHeader = "";
    //public string[] choices = new string[0];
    [System.Serializable]
    public class Choice
    {
        public string choiceStr = "";
        public int gotoLine = 0;
    }
    public Choice[] choices = new Choice[0];

    [XmlIgnore] private int currentChoiceID = 0;

    public DialogueChoice()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
		SELECTED_CHOICE = -1;
        //Startup
        float lastTimeScale = Time.timeScale;
        Time.timeScale = 1 / 50000f;


        
        string str = promptHeader;
        str = DialogueHandler.VariablesToString(str);

        

        handle.message.text = str;
        int currentChoiceID = 0;
        bool selectionPressed = false;
        bool continueMe = true;

        for (int i = 0; i < handle.choiceOverlays.Length; i++)
        {
            handle.choiceOverlays[i].SetActive(true);
			if(i >= choices.Length)
				handle.choiceOverlays[i].SetActive(false);
			TMPro.TextMeshProUGUI txt = handle.choiceOverlays[i].transform.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>();
			
			if(i < choices.Length)
				txt.text = choices[i].choiceStr;
        }
		handle.SetMessageY(0f);
		EventSystem.current.SetSelectedGameObject(handle.choiceOverlays[0].gameObject);

		while(SELECTED_CHOICE == -1)
		{
			yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
		}
        
        //Clear choices after the choice
        for (int i = 0; i < handle.choiceOverlays.Length; i++)
        {
            handle.choiceOverlays[i].SetActive(false);
        }

        Time.timeScale = lastTimeScale;

        handle.GotoLine(choices[SELECTED_CHOICE].gotoLine);
        
        completed = true;
    }

    private int lastChoiceCount = 0;
    private int t = 0;  //t is set per OnGUI
    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Top prompt");
        promptHeader = GUILayout.TextField(promptHeader);
        t = this.IntField("choice count", t);
        if(t != lastChoiceCount)  //Change size
        {
            choices = null;
            choices = new Choice[t];
            for(int i = 0; i < choices.Length; i++)
            {
                choices[i] = new Choice();
            }
            lastChoiceCount = t;
        }

        for(int i = 0; i < choices.Length; i++)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Choice " + i);
            
            choices[i].choiceStr = GUILayout.TextField(choices[i].choiceStr);
            choices[i].gotoLine = this.IntField("Goto", choices[i].gotoLine);
            GUILayout.EndHorizontal();
        }
    }

    public override string ToString()
    {
        return "Set choice";
    }
}