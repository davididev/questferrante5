﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class DialogueTeleportActor : DialogueItem {

	public static bool IsTeleporting = false;
    public string actorToMove;
    public Vector3 positionToMoveTo;
    public string actorToMoveTo;
    public string sceneToMoveTo;
    public bool moveViaActor;

    public enum TRANSITION_TYPE { Instant, Fade, SwordShield};
    public TRANSITION_TYPE trans = TRANSITION_TYPE.Instant;


    public DialogueTeleportActor()
    {
		this.breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
		this.breakpoint = true;
        if (sceneToMoveTo == "")  //Teleporting in scene
        {
            Actor a1;
            if (Actor.actors.TryGetValue(actorToMove, out a1))
            {
                Vector3 newPos = positionToMoveTo;
                if (moveViaActor)
                {
                    Actor a2;
                    if (Actor.actors.TryGetValue(actorToMoveTo, out a2))
                    {
                        newPos = a2.transform.position;
                    }
                    else
                        Debug.Log("<color=red>Actor to move to (" + actorToMoveTo + ") not found.  Sorry.</color>");
                }


                CharacterController cc = a1.GetComponent<CharacterController>();
                if (cc != null)
                    cc.enabled = false;

                a1.transform.position = newPos;

                if (cc != null)
                    cc.enabled = true;
            }
            else
            {
                Debug.Log("<color=red>Actor to move (" + actorToMove + ") not found.  Sorry.</color>");
            }
        }
  
        if(sceneToMoveTo != "")  //Teleporting outside of scene (assume hero)
        {
            IsTeleporting = true;
            AsyncOperation op = SceneManager.LoadSceneAsync(sceneToMoveTo);
            //Pre load
            Transform playerTrans = GameObject.FindGameObjectWithTag("Player").transform;

			float loadTime = 0f;

            if (trans == TRANSITION_TYPE.Fade)
            {
                while(op.progress < 0.9f)
                {
                    Color c = new Color(0f, 0f, 0f, (op.progress));
                    this.handle.SetFlashColor(c);
                    yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
                    loadTime += Time.unscaledDeltaTime;
                }

                handle.SetFlashColor(Color.black);
                
            }
            if(trans == TRANSITION_TYPE.SwordShield)
            {
                while (op.progress < 0.9f)
                {
                    handle.toBattleImage.localScale = new Vector3(op.progress, op.progress, op.progress);
                    yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
                    loadTime += Time.unscaledDeltaTime;
                }
                handle.toBattleImage.localScale = Vector3.one;
                
            }
			while(op.isDone == false)
			{
				yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
			}
            
            Debug.Log("Bork");
            yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
			playerTrans.GetComponent<Rigidbody2D>().position = positionToMoveTo;
            //Post load
            if (trans == TRANSITION_TYPE.Fade)
            {
                float t = loadTime;
                while(t > 0f)
                {
                    float p = t / loadTime;
                    Color c = new Color(0f, 0f, 0f, (p));
                    this.handle.SetFlashColor(c);
                    yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
                    t -= Time.unscaledDeltaTime;
                }
                handle.SetFlashColor(Color.clear);
            }

            if (trans == TRANSITION_TYPE.SwordShield)
            {
                float t = loadTime;
                while (t > 0f)
                {
                    float p = t / loadTime;
                    handle.toBattleImage.localScale = new Vector3(p, p, p);
                    yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
                    t -= Time.unscaledDeltaTime;
                }
                handle.toBattleImage.localScale = Vector3.zero;
            }

			
        }
		
		GameDataHolder.instance.GetCurrentHero().currentScene = sceneToMoveTo;
			GameDataHolder.instance.GetCurrentHero().currentPosition = positionToMoveTo;
		IsTeleporting = false;
        completed = true;
    }



    public override void DrawEditor(Rect verticalGroup)
    {
        base.DrawEditor(verticalGroup);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Actor to move");
        actorToMove = GUILayout.TextField(actorToMove);
        GUILayout.EndVertical();

        moveViaActor = GUILayout.Toggle(moveViaActor, "Teleport to empty actor");
        if(moveViaActor == true)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Empty Actor to move to:");
            actorToMoveTo = GUILayout.TextField(actorToMoveTo);
            GUILayout.EndVertical();
        }

        if(moveViaActor == false)
        {
            GUILayout.Label("Position to move to:");
            GUILayout.BeginHorizontal();
            Vector3 v = positionToMoveTo;
            v.x = FloatField("X: ", v.x);
            v.y = FloatField("Y: ", v.y);
            v.z = FloatField("Z: ", v.z);
            positionToMoveTo = v;

            GUILayout.EndVertical();
        }

        if (GUILayout.Button("Clear Scene"))
            sceneToMoveTo = "";
        GUILayout.Label("Scene to move to (leave blank if nowhere)");
        sceneToMoveTo = GUILayout.TextField(sceneToMoveTo);

        if(sceneToMoveTo != "")
        {
            trans = (TRANSITION_TYPE) this.EnumPopup("Transition type", trans);
        }
    }

}
