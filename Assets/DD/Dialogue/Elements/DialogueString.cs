﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class DialogueString : DialogueItem {

    public string characterName = "";
    public string textToDisplay = "";
    public bool cantSkip = false, closeAfter = false;

    private const float WIDTH = 400f;
    private const float HEIGHT = 300f;
    public static string lastCharacterName = "";

    public DialogueString()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();

        handle.SetMessageY(0f);
        
        
        string str;
        if (characterName != "")
            str = characterName + "\n" + textToDisplay;
        else
            str = textToDisplay;
        DialogueHandler.dialogueStringSrc.clip = Resources.Load<AudioClip>("Audio/TextScroll");

        str = DialogueHandler.VariablesToString(str);
		str = str.Replace("#AU", "<sprite=\"Emoticans.fw\" index=0>");
		str = str.Replace("#AR", "<sprite=\"Emoticans.fw\" index=1>");
		str = str.Replace("#AD", "<sprite=\"Emoticans.fw\" index=2>");
		str = str.Replace("#AL", "<sprite=\"Emoticans.fw\" index=3>");
		str = str.Replace("#SO", "<sprite=\"Emoticans.fw\" index=4>");
		str = str.Replace("#SM", "<sprite=\"Emoticans.fw\" index=5>");
        int i = 0;
        while (i < str.Length)
        {
            if (DialogueHandler.dialogueStringSrc.isPlaying == false)
            {
                DialogueHandler.dialogueStringSrc.Play();
            }
            if (i >= str.Length)
                i = str.Length - 1;

            if (str[i] == '<')
            {
                i = str.IndexOf('>', i) + 1;
            }
            
            handle.message.text = str.Substring(0, i);
            yield return new WaitForSecondsNoTimeScale(0.05f);
            i++;

            if (cantSkip == false)
            {
				if(Input.inputString != "" || Input.GetMouseButton(0) == true)
					i = str.Length;
            }

            
        }
		
		

        handle.message.text = str;


		//While it is up
		while(Input.inputString != "" || Input.GetMouseButton(0) == true)
		{
			yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
		}


        //Wait until it's down again
        while(Input.inputString == "" && Input.GetMouseButton(0) == false)
        {
            yield return new WaitForSecondsNoTimeScale(Time.unscaledDeltaTime);
        }

        yield return new WaitForSecondsNoTimeScale(0.05f);

        if (closeAfter == true)
        {
            handle.SetMessageY(-400f);
        }

        completed = true;



    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Character name");
        characterName = GUILayout.TextField(characterName);
        GUILayout.Label("Text to display");
        textToDisplay = GUILayout.TextField(textToDisplay);


        cantSkip = GUILayout.Toggle(cantSkip, "Can't Skip?");
        closeAfter = GUILayout.Toggle(closeAfter, "Close box after?");
    }

    //Needed for the editor.  Returns the object tpye
    public override string ToString()
    {
        return "Talk (" + characterName + ")";
    }
}
