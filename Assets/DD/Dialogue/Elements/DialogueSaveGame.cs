﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml.Serialization;


[System.Serializable]
public class DialogueSaveGame : DialogueItem
{
    public override IEnumerator Run()
    {
		yield return base.Run();
		
		bool failure = true;
		Vector2 savePos = Vector2.zero;
		while(failure == true)
		{
			Rigidbody2D rigid = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>();
			if(rigid != null)
			{
				savePos = rigid.position;
				failure = false;
			}
			yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
		}
			
		
		GameDataHolder.instance.GetCurrentHero().currentScene = SceneManager.GetActiveScene().name;
		GameDataHolder.instance.GetCurrentHero().currentPosition = savePos;
		GameDataHolder.SaveFile(GameDataHolder.fileID);
		
		completed = true;
	}
	
	public override void DrawEditor(Rect verticalGroup)
    {
		base.DrawEditor(verticalGroup);
		GUILayout.Label("This thing has no parameters.");
	}
}
