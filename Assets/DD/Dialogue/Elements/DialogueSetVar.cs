﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DialogueSetVar : DialogueItem
{
    public string varNameToSet = "%varname";
    public float varValue = 0f;
    public enum ALTER_TYPE { SET, ADD, SUBTRACT, MULTIPLY, DIVIDE };
    public ALTER_TYPE alterType = ALTER_TYPE.SET;

    public override IEnumerator Run()
    {
        yield return base.Run();
        if (alterType == ALTER_TYPE.SET)
        {
            DialogueHandler.SetVar(varNameToSet, varValue);
        }
        else
        {
            float f = DialogueHandler.GetVar(varNameToSet);
            if (alterType == ALTER_TYPE.ADD)
                f = f + varValue;
            if (alterType == ALTER_TYPE.SUBTRACT)
                f = f - varValue;
            if (alterType == ALTER_TYPE.MULTIPLY)
                f = f * varValue;
            if (alterType == ALTER_TYPE.DIVIDE)
                f = f / varValue;
			
			DialogueHandler.SetVar(varNameToSet, f);
        }
        completed = true;
        yield return null;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Variable:");
        varNameToSet = GUILayout.TextField(varNameToSet);
        varValue = this.FloatField("Value", varValue);
        alterType = (ALTER_TYPE) this.EnumPopup("Alter by", alterType);
    }

    public override string ToString()
    {
		if (alterType == ALTER_TYPE.ADD)
			return "(+) variable";
		if (alterType == ALTER_TYPE.SUBTRACT)
			return "(-) variable";
		if (alterType == ALTER_TYPE.MULTIPLY)
			return "(x) variable";
		if (alterType == ALTER_TYPE.DIVIDE)
			return "(/) variable";
		
		return "Set variable";
    }
}
