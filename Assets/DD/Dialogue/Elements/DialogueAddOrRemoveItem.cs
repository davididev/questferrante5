﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueAddOrRemoveItem : DialogueItem
{
    public int addItem = 1;  //Numbers to add
    public char itemID = '0';
    public override IEnumerator Run()
    {
        yield return base.Run();

        GameData.Hero h = GameDataHolder.instance.GetCurrentHero();

        DialogueHandler.SetVar("%exist_before", (float)GameDataHolder.instance.ItemExists(itemID));

        GameDataHolder.instance.AddItem(itemID, addItem);

        DialogueHandler.SetVar("%exist_after", (float)GameDataHolder.instance.ItemExists(itemID));

        yield return null;

        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        addItem = this.IntField("Offset amount", addItem);

        GUILayout.Label("Item ID");
        string s = GUILayout.TextField(itemID.ToString(), 1);

        itemID = s[0];

    }
}
