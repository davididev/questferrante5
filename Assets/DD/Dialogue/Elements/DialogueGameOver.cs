﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogueGameOver : DialogueItem
{
    public override IEnumerator Run()
    {
        yield return base.Run();
        handle.flashImage.sprite = Resources.Load<Sprite>("Game-Over") as Sprite;
        handle.flashImage.color = Color.white;
        yield return new WaitForSeconds(4f);
        completed = true;
        SceneManager.LoadScene("Title");
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        base.DrawEditor(verticalGroup);
        GUILayout.Label("This thing has no parameters.");
    }
}
