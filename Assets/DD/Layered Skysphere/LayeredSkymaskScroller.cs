﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayeredSkymaskScroller : MonoBehaviour
{
    public static float Layer1ScrollSpeed = 0.1f, Layer2ScrollSpeed = -0.1f, BaseLayerScrollSpeed = 0f;
    public float baseLayerStartingSpeed = 0f, layer1StartingSpeed = 0.1f, layer2StartingSpeed = -0.1f;
	public float baseLayerStartingAngle = 0f, layer1StartingAngle = 0f, layer2StartingAngle = 180;
    
	private float offsetLayerBase, offsetLayer1, offsetLayer2;
    // Start is called before the first frame update
    void Start()
    {
        
            Layer1ScrollSpeed = layer1StartingSpeed;
        
            Layer2ScrollSpeed = layer2StartingSpeed;
            BaseLayerScrollSpeed  = baseLayerStartingSpeed;
		
		offsetLayerBase = baseLayerStartingAngle;
		offsetLayer1 = layer1StartingAngle;
		offsetLayer2 = layer2StartingAngle;
    }

    // Update is called once per frame
    void Update()
    {

		offsetLayerBase += BaseLayerScrollSpeed * Time.deltaTime;
		if(offsetLayer1 < 0f)
			offsetLayer1 += 360f;
		if(offsetLayer1 > 360f)
			offsetLayer1 -= 360f;


        offsetLayer1 += Layer1ScrollSpeed * Time.deltaTime;
		if(offsetLayer1 < 0f)
			offsetLayer1 += 360f;
		if(offsetLayer1 > 360f)
			offsetLayer1 -= 360f;

        offsetLayer2 += Layer2ScrollSpeed * Time.deltaTime;
		if(offsetLayer2 < 0f)
			offsetLayer2 += 360f;
		if(offsetLayer2 > 360f)
			offsetLayer2 -= 360f;
		
		RenderSettings.skybox.SetFloat("_Rotation", offsetLayerBase);
		RenderSettings.skybox.SetFloat("_Rotation2", offsetLayer1);
		RenderSettings.skybox.SetFloat("_Rotation3", offsetLayer2);
    }
}
